import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Hidden, IconButton, Divider, List } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

import { StyledDrawer, Row, Logo } from './styles';

import dashboardIcon from '~/assets/icons/dashboard-icon.png';
import groupsIcon from '~/assets/icons/groups-black-48dp.svg';
import coursesIcon from '~/assets/icons/lesson-two.png';
// import chatsIcon from '~/assets/icons/chats.png';
import formIcon from '~/assets/icons/form.png';
// import configIcon from '~/assets/icons/config.png';
import Item from './Components/Item';

import logoImg from '~/assets/images/log.svg';
import { useAuth } from '~/hooks/Auth';

const DrawerMenu = ({ handleDrawer, visibleDrawer, drawerWidth, location }) => {
  const [itemMenu, setItemMenu] = useState(0);
  const [itemSubMenu, setItemSubMenu] = useState(null);
  const { user } = useAuth();
  const currentLink = location.pathname.substr(location.pathname.indexOf('/'));

  const currentLinkSplit = location.pathname
    .substr(location.pathname.indexOf('/'))
    .split('/');

  let listMenu = [];
  if (user.is_admin) {
    listMenu = [
      {
        id: 1,
        name: 'Dashboard',
        link: '/dashboard',
        icon: dashboardIcon,
      },
      {
        id: 2,
        name: 'Categorias',
        link: '/dashboard/categories',
        icon: coursesIcon,
      },
      {
        id: 3,
        name: 'Sub Categorias',
        link: '/dashboard/sub-categories',
        icon: formIcon,
      },
      {
        id: 4,
        name: 'Sub Grupos',
        link: '/dashboard/sub-groups',
        icon: formIcon,
      },
      {
        id: 5,
        name: 'Tags',
        link: '/dashboard/tags',
        icon: formIcon,
      },
      {
        id: 6,
        name: 'Questões',
        link: '/dashboard/questions',
        icon: coursesIcon,
      },
      {
        id: 7,
        name: 'Simulados',
        link: '/dashboard/categories-simulates',
        icon: coursesIcon,
      },
      // {
      //   id: 2,
      //   name: 'Simulado Principal',
      //   link: '/dashboard/simulates-main',
      //   icon: coursesIcon,
      // },
      {
        id: 8,
        name: 'Usuários',
        icon: groupsIcon,
        link: '/dashboard/users',
      },
      // {
      //   id: 3,
      //   name: language ? 'Clients' : 'Clientes',
      //   icon: groupsIcon,
      //   link: '/dashboard/clients',
      // },
      // {
      //   id: 4,
      //   name: language ? 'Courses' : 'Cursos',
      //   icon: coursesIcon,
      //   link: '/dashboard/courses',
      // },
      // {
      //   id: 5,
      //   name: 'Chats',
      //   icon: chatsIcon,
      //   link: '/dashboard/chats',
      // },
      // {
      //   id: 6,
      //   name: 'Feeds',
      //   icon: formIcon,
      //   link: '/dashboard/feeds',
      // },
      // {
      //   id: 7,
      //   name: 'Configurações',
      //   icon: configIcon,
      //   link: '/dashboard/settings',
      // },
      // {
      //   id: 5,
      //   name: language ? 'Lessons' : 'Aulas',
      //   icon: coursesIcon,
      //   link: '/dashboard/lessons',
      // },
    ];
  }

  function checkSelected(itemLink) {
    if (itemLink) {
      if (currentLinkSplit[currentLinkSplit.length - 1] === 'add') {
        const newCurrentLink = currentLink.substr(
          0,
          currentLink.length -
            currentLinkSplit[currentLinkSplit.length - 1].length -
            1
        );

        return itemLink === newCurrentLink;
      }

      if (currentLinkSplit[currentLinkSplit.length - 2] === 'edit') {
        const newCurrentLink = currentLink.substr(
          0,
          currentLink.length -
            (currentLinkSplit[currentLinkSplit.length - 2].length +
              currentLinkSplit[currentLinkSplit.length - 1].length +
              2)
        );

        return itemLink === newCurrentLink;
      }

      return itemLink === currentLink;
    }

    return false;
  }

  const ItemsDrawer = () => {
    return (
      <>
        <Row>
          <Logo>
            <img alt="EAD" src={logoImg} />
          </Logo>
          <IconButton onClick={handleDrawer}>
            <ChevronLeftIcon color="secondary" />
          </IconButton>
        </Row>
        <Divider />

        <List>
          {listMenu.map(item => (
            <Item
              key={item.id}
              item={item}
              checkSelected={checkSelected}
              itemMenu={itemMenu}
              setItemMenu={setItemMenu}
              itemSubMenu={itemSubMenu}
              setItemSubMenu={setItemSubMenu}
              icon={item.icon}
            />
          ))}
        </List>
      </>
    );
  };

  return (
    <>
      <Hidden smDown>
        <StyledDrawer
          variant="persistent"
          anchor="left"
          ModalProps={{
            disableScrollLock: true,
          }}
          drawerwidth={drawerWidth}
          open={visibleDrawer}
        >
          {ItemsDrawer()}
        </StyledDrawer>
      </Hidden>

      <Hidden mdUp>
        <StyledDrawer
          variant="temporary"
          anchor="left"
          drawerwidth={drawerWidth}
          open={visibleDrawer}
          onClose={handleDrawer}
          ModalProps={{
            keepMounted: true,
            disableScrollLock: true,
          }}
        >
          {ItemsDrawer()}
        </StyledDrawer>
      </Hidden>
    </>
  );
};

export default withRouter(DrawerMenu);

DrawerMenu.propTypes = {
  handleDrawer: PropTypes.func.isRequired,
  visibleDrawer: PropTypes.bool.isRequired,
  drawerWidth: PropTypes.number.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
};
