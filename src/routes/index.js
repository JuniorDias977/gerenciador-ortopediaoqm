import React from 'react';
import { Switch } from 'react-router-dom';
import RouteWrapper from './Route';

import SignIn from '~/pages/SignIn';
import Dashboard from '~/pages/Dashboard';
import Users from '~/pages/Users';
import Recover from '~/pages/Recover';
import Categories from '~/pages/Categories';
import SubCategories from '~/pages/SubCategories';
import SubGroups from '~/pages/SubGroups';
import Questions from '~/pages/Questions';
import Tags from '~/pages/Tags';
import SubCategoriesByCategory from '~/pages/Categories/Components/SubCategories';
import SubGroupBySubCategory from '~/pages/SubCategories/Components/SubGroups';
import CategoriesSimulate from '~/pages/CategoriesSimulate';
import SimulatesByCategory from '~/pages/CategoriesSimulate/Components/Simulates';
import SimulatesQuestions from '~/pages/CategoriesSimulate/Components/Simulates/Components/SimulatesQuestions';
import SimulatesMain from '~/pages/SimulatesMain';
import SimulatesMainQuestions from '~/pages/SimulatesMain/Components/SimulatesQuestions';

const Routes = () => {
  return (
    <Switch>
      <RouteWrapper path="/" exact component={SignIn} />
      <RouteWrapper path="/recover" exact component={Recover} />
      <RouteWrapper path="/dashboard" isPrivate exact component={Dashboard} />
      <RouteWrapper
        path="/dashboard/categories"
        isPrivate
        exact
        component={Categories}
      />
      <RouteWrapper
        path="/dashboard/categories-simulates"
        isPrivate
        exact
        component={CategoriesSimulate}
      />
      <RouteWrapper
        path="/dashboard/simulates-main"
        isPrivate
        component={SimulatesMain}
      />
      <RouteWrapper
        path="/dashboard/simulates/:id"
        isPrivate
        component={SimulatesByCategory}
      />
      <RouteWrapper
        path="/dashboard/simulate-main-questions/:id"
        isPrivate
        component={SimulatesMainQuestions}
      />
      <RouteWrapper
        path="/dashboard/simulate-questions/:id"
        isPrivate
        component={SimulatesQuestions}
      />
      <RouteWrapper
        path="/dashboard/sub-categories/:id"
        isPrivate
        component={SubCategoriesByCategory}
      />
      <RouteWrapper
        path="/dashboard/sub-categories"
        isPrivate
        exact
        component={SubCategories}
      />
      <RouteWrapper path="/dashboard/users" isPrivate exact component={Users} />
      <RouteWrapper
        path="/dashboard/sub-groups"
        isPrivate
        exact
        component={SubGroups}
      />
      <RouteWrapper
        path="/dashboard/sub-groups/:id"
        isPrivate
        component={SubGroupBySubCategory}
      />
      <RouteWrapper
        path="/dashboard/questions"
        isPrivate
        exact
        component={Questions}
      />
      <RouteWrapper path="/dashboard/tags" isPrivate exact component={Tags} />
      {/* <RouteWrapper
        path="/dashboard/clients"
        isPrivate
        exact
        component={Clients}
      />
      <RouteWrapper
        path="/dashboard/profile"
        isPrivate
        exact
        component={Profile}
      /> */}
      {/* <RouteWrapper
        path="/dashboard/lessons"
        isPrivate
        exact
        component={Lesson}
      /> */}
      {/* <RouteWrapper
        path="/dashboard/settings"
        isPrivate
        exact
        component={Settings}
      /> */}
    </Switch>
  );
};

export default Routes;
