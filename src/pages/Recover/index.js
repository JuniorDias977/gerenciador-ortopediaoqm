import React, { useCallback } from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Grid, TextField } from '@material-ui/core';

import {
  StyledContainer,
  StyledGridItem,
  StyledSubmitButton,
  StyledForm,
  ErrorMessageText,
} from './styles';

import colors from '~/styles/colors';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import history from '~/services/history';

import firebase from '~/services/firebase';

import logoImg from '~/assets/images/log.svg';

const SignIn = () => {
  const { loading, setLoading, language } = useAuth();
  const { addToast } = useToast();

  const schema = Yup.object().shape({
    email: Yup.string().required(language ? 'Enter email.' : 'Informe o email'),
  });

  const handleLogin = useCallback(
    async data => {
      try {
        firebase.auth().sendPasswordResetEmail(data.email);
        addToast({
          type: 'success',
          title: 'Sucesso!',
          description: 'Verifique seu email.',
        });
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
          description: 'Verifique seu email e tente novamente',
        });
        setLoading(false);
      }
    },
    [addToast, setLoading, language]
  );

  return (
    <StyledContainer maxWidth="sm">
      <Formik
        validationSchema={schema}
        initialValues={{
          email: '',
        }}
        onSubmit={handleLogin}
      >
        {({ handleChange, handleSubmit, touched, errors }) => (
          <StyledForm>
            <img alt="Coupons" width="200" src={logoImg} />
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <StyledGridItem item sm marginTop={30}>
                <TextField
                  fullWidth
                  id="email"
                  name="email"
                  style={{ backgroundColor: 'white', borderRadius: 8 }}
                  required
                  onChange={handleChange}
                  placeholder="Email"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />

                {errors.email && touched.email ? (
                  <ErrorMessageText>{errors.email}</ErrorMessageText>
                ) : null}
              </StyledGridItem>

              <center>
                <StyledGridItem item marginTop={70}>
                  <StyledSubmitButton
                    variant="contained"
                    background={colors.blueButton}
                    color={colors.white}
                    type="submit"
                    onClick={handleSubmit}
                  >
                    {loading ? `${'Aguarde...'}` : `${'Recuperar'}`}
                  </StyledSubmitButton>
                </StyledGridItem>
                <StyledGridItem item marginTop={24}>
                  <StyledSubmitButton
                    variant="contained"
                    background={colors.blueButton}
                    color={colors.white}
                    type="button"
                    onClick={() => history.goBack()}
                  >
                    Voltar para login
                  </StyledSubmitButton>
                </StyledGridItem>

                {/* <StyledGridItem item>
                  <StyledSubmitButton
                    variant="outlined"
                    color="primary"
                    type="button"
                  >
                    Recuperar a senha
                  </StyledSubmitButton>
                </StyledGridItem> */}
              </center>
            </Grid>
          </StyledForm>
        )}
      </Formik>
    </StyledContainer>
  );
};

export default SignIn;
