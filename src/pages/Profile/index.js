/* eslint-disable react/jsx-boolean-value */
/* eslint-disable no-plusplus */
/* eslint-disable array-callback-return */
import React, { useCallback, useState } from 'react';
import { Formik, Form } from 'formik';
import { Grid, DialogActions, Button, MenuItem } from '@material-ui/core';
import * as Yup from 'yup';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import {
  TextFieldStyled,
  ErrorMessageText,
  Container,
  PaperStyled,
} from './styles';

const Dashboard = () => {
  const { addToast } = useToast();
  const { language, user } = useAuth();
  const [loading, setLoading] = useState(false);
  const [item, setItem] = useState(user);

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o nome da Matriz'
    ),
    email: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o email do Usuário'
    ),
    type: Yup.string().required(
      language
        ? 'Enter the permission of the user.'
        : 'Informe a permissão do Usuário'
    ),
  });

  const levels = [
    {
      value: 'admin',
      label: language ? 'Adm' : 'Admin',
    },
    {
      value: 'user',
      label: language ? 'Default' : 'Padrão',
    },
  ];

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.id) {
        api
          .put(`users/${item.id}`, {
            ...data,
          })
          .then(resp => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            setLoading(false);
            setItem(resp.data);
            localStorage.setItem('@finnancier:user', JSON.stringify(resp.data));
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      }
    },
    [addToast, item, language]
  );

  return (
    <Container>
      <PaperStyled>
        {loading && !user ? (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        ) : (
          <Formik
            enableReinitialize
            validationSchema={schema}
            initialValues={{
              name: item && item.name ? item.name : '',
              email: item && item.email ? item.email : '',
              password: '',
              active: item && item.active ? item.active : false,
              type: item && item.type ? item.type : false,
            }}
            onSubmit={handleSaveClasse}
          >
            {({ values, handleChange, handleSubmit, errors, touched }) => (
              <Form>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.name}
                      fullWidth
                      id="name"
                      name="name"
                      required
                      onChange={handleChange}
                      label={language ? 'Name' : 'Nome'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.name && touched.name ? (
                      <ErrorMessageText>{errors.name}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.email}
                      fullWidth
                      id="email"
                      name="email"
                      required
                      onChange={handleChange}
                      label="E-mail"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.email && touched.email ? (
                      <ErrorMessageText>{errors.email}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.password}
                      type="password"
                      fullWidth
                      id="password"
                      name="password"
                      required
                      onChange={handleChange}
                      label={language ? 'Password' : 'Senha'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.password && touched.password ? (
                      <ErrorMessageText>{errors.password}</ErrorMessageText>
                    ) : null}
                  </Grid>

                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.type}
                      fullWidth
                      id="type"
                      name="type"
                      required
                      select
                      disabled
                      onChange={handleChange}
                      label={language ? 'Level user' : 'Nível de permissão'}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    >
                      {levels && levels.length
                        ? levels.map(level => (
                            <MenuItem key={level.value} value={level.value}>
                              {level.label}
                            </MenuItem>
                          ))
                        : null}
                    </TextFieldStyled>
                    {errors.level && touched.level ? (
                      <ErrorMessageText>{errors.level}</ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                {item && (
                  <Grid item xs={12}>
                    <TextFieldStyled
                      value={values.active}
                      fullWidth
                      id="active"
                      name="active"
                      required
                      onChange={handleChange}
                      label="active"
                      select
                      disabled
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      SelectProps={{
                        native: true,
                      }}
                    >
                      <option key={true} value={true}>
                        {language ? 'Active' : 'Ativo'}
                      </option>
                      <option key={false} value={false}>
                        {language ? 'Inactive' : 'Inativo'}
                      </option>
                    </TextFieldStyled>
                  </Grid>
                )}
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <Grid container justify="flex-end">
                      <DialogActions>
                        <Button
                          onClick={() => handleSubmit()}
                          color="primary"
                          variant="contained"
                          disabled={loading}
                        >
                          {language ? 'Save' : 'Salvar'}
                        </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        )}
      </PaperStyled>
    </Container>
  );
};

export default Dashboard;
