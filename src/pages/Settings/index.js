/* eslint-disable react/jsx-boolean-value */
/* eslint-disable no-plusplus */
/* eslint-disable array-callback-return */
import React, { useCallback, useState, useEffect } from 'react';
import { Formik, Form } from 'formik';
import { Grid, DialogActions, Button, TextField } from '@material-ui/core';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import {
  TextFieldStyled,
  ErrorMessageText,
  Container,
  PaperStyled,
  NumberFormatStyled,
} from './styles';

const Dashboard = () => {
  const { addToast } = useToast();
  const { language } = useAuth();
  const [loading, setLoading] = useState(false);
  const [item, setItem] = useState();

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.id) {
        api
          .put(`settings`, {
            ...data,
            value: data.value * 100,
          })
          .then(resp => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            setLoading(false);
            setItem(resp.data);
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      }
    },
    [addToast, item, language]
  );

  const loadSettings = useCallback(async () => {
    try {
      const resp = await api.get('/settings');
      setItem(resp.data);
    } catch (err) {
      addToast({
        type: 'error',
        title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
      });
    }
  }, [addToast, language]);

  useEffect(() => {
    loadSettings();
  }, [loadSettings]);

  return (
    <Container>
      <PaperStyled>
        {loading ? (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        ) : (
          <Formik
            enableReinitialize
            initialValues={{
              onDigitalKey: item && item.onDigitalKey ? item.onDigitalKey : '',
              months: item && item.months ? item.months : 0,
              value: item && item.value ? item.value / 100 : 0,
              email: item && item.email ? item.email : '',
            }}
            onSubmit={handleSaveClasse}
          >
            {({
              values,
              handleChange,
              handleSubmit,
              errors,
              touched,
              setFieldValue,
            }) => (
              <Form>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.onDigitalKey}
                      fullWidth
                      id="onDigitalKey"
                      name="onDigitalKey"
                      required
                      onChange={handleChange}
                      label="Chave de notificações (One signal)"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.onDigitalKey && touched.onDigitalKey ? (
                      <ErrorMessageText>{errors.onDigitalKey}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.email}
                      fullWidth
                      id="email"
                      name="email"
                      required
                      onChange={handleChange}
                      label="Email para notificação"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.email && touched.email ? (
                      <ErrorMessageText>{errors.email}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextFieldStyled
                      value={values.months}
                      fullWidth
                      id="months"
                      name="months"
                      required
                      type="number"
                      onChange={handleChange}
                      label="Meses de assinatura"
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.months && touched.months ? (
                      <ErrorMessageText>{errors.months}</ErrorMessageText>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} sm={12} md={4}>
                    <NumberFormatStyled
                      customInput={TextField}
                      thousandSeparator="."
                      decimalSeparator=","
                      decimalScale={2}
                      fixedDecimalScale
                      variant="outlined"
                      id="value"
                      name="value"
                      value={values.value}
                      onValueChange={val => {
                        setFieldValue('value', val.floatValue || 0);
                      }}
                      label="Valor R$"
                      margin="normal"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.value && touched.value ? (
                      <ErrorMessageText>{errors.value}</ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container>
                  <Grid item xs={12} sm={12} md={12}>
                    <Grid container justify="flex-end">
                      <DialogActions>
                        <Button
                          onClick={() => handleSubmit()}
                          color="primary"
                          variant="contained"
                          disabled={loading}
                        >
                          {language ? 'Save' : 'Salvar'}
                        </Button>
                      </DialogActions>
                    </Grid>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        )}
      </PaperStyled>
    </Container>
  );
};

export default Dashboard;
