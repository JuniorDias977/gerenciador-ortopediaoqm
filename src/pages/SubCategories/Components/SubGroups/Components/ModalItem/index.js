/* eslint-disable array-callback-return */
/* eslint-disable func-names */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import Select from 'react-select';
import { format } from 'date-fns';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core';

import firebase from '~/services/firebase';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';
import { useSubCategories } from '~/hooks/SubCategories';

import { TextFieldStyled, ErrorMessageText } from './styles';

const ModalItem = ({
  handleCloseModal,
  open,
  item = null,
  getItens,
  categoryId,
}) => {
  const { subCategories, getSubCategories } = useSubCategories();
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();
  const [categoryData, setCategoryData] = useState([]);

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language
        ? 'Enter the name of the sub group.'
        : 'Informe o título do sub grupo'
    ),
  });

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.key) {
        firebase
          .firestore()
          .collection('sub_groups')
          .doc(item.key)
          .update({
            ...data,
            sub_category: categoryData[0],
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('sub_groups')
          .add({
            ...data,
            sub_category: categoryData[0],
            date: format(new Date(), 'yyyy-MM-dd')
              .concat(' ')
              .concat(format(new Date(), 'HH:mm:ss')),
            active: true,
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            addToast({
              type: 'error',
              title: 'Tente novamente',
            });
            setLoading(false);
          });
      }
    },
    [addToast, getItens, handleCloseModal, item, language, categoryData]
  );

  useEffect(() => {
    getSubCategories();
  }, [getSubCategories]);

  useEffect(() => {
    async function getCategoriesData() {
      try {
        setLoading(true);
        const arr = [];
        subCategories.map(category => {
          if (category.active && category.key === categoryId) {
            const objData = {
              value: category.key,
              label: category.name,
            };
            arr.push(objData);
          }
        });
        setCategoryData(arr);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    }
    getCategoriesData();
  }, [addToast, language, subCategories, categoryId]);

  return (
    <Dialog aria-labelledby="customized-dialog-title" open={open}>
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">
            {language ? 'Sub Group' : 'Sub Grupo'}{' '}
          </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            name: item && item.name ? item.name : '',
            sub_category: item && item.sub_category ? item.sub_category : '',
          }}
          onSubmit={handleSaveClasse}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            errors,
            touched,
            setFieldValue,
          }) => (
            <Form>
              <Grid container>
                <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                  <TextFieldStyled
                    value={values.name}
                    fullWidth
                    id="name"
                    name="name"
                    required
                    onChange={handleChange}
                    label={language ? 'Name' : 'Título do sub grupo'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.name && touched.name ? (
                    <ErrorMessageText>{errors.name}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 8, zIndex: 1000 }}
                >
                  <span>Selecione a sub categoria</span>
                  <Select
                    options={categoryData}
                    value={values.sub_category || categoryData[0]}
                    isDisabled
                    placeholder="Selecione a sub categoria"
                    style={{ background: 'white', zIndex: 999 }}
                    label="Selecione a sub categoria"
                    onChange={e => {
                      setFieldValue('sub_category', e);
                    }}
                  />
                </Grid>
              </Grid>
              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={handleChange}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
  categoryId: PropTypes.string.isRequired,
};

export default ModalItem;
