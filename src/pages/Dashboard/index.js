/* eslint-disable react/jsx-boolean-value */
/* eslint-disable radix */
/* eslint-disable prefer-const */
/* eslint-disable array-callback-return */
import React, { useEffect, useState, useCallback } from 'react';
import { Formik, Form } from 'formik';
import { Grid, DialogActions, Button } from '@material-ui/core';

import firebase from '~/services/firebase';

import { Container, PaperStyled, TextFieldStyled } from './styles';
import { useToast } from '~/hooks/Toast';

const Item = () => {
  const [data, setData] = useState();
  const { addToast } = useToast();

  useEffect(() => {
    firebase
      .firestore()
      .collection('settings')
      .onSnapshot(querySnapshot => {
        let categoriesArr = [];
        querySnapshot.forEach(documentSnapshot => {
          categoriesArr.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        setData(categoriesArr[0]);
      });
  }, []);

  const handleSaveSettings = useCallback(
    dt => {
      if (data) {
        firebase
          .firestore()
          .collection('settings')
          .doc(data.key)
          .update({
            free: !!(dt.free === 'true' || dt.free === true),
          })
          .then(() => {
            addToast({
              type: 'success',
              title: 'Salvo com sucesso',
            });
          });
      }
    },
    [data, addToast]
  );

  return (
    <Container>
      <PaperStyled>
        <Formik
          enableReinitialize
          initialValues={{
            free: data ? data.free : false,
          }}
          onSubmit={handleSaveSettings}
        >
          {({ values, handleChange, handleSubmit }) => (
            <Form>
              <Grid container>
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.free}
                    fullWidth
                    id="free"
                    name="free"
                    required
                    onChange={handleChange}
                    label="Status do aplicativo"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      Liberado para todos
                    </option>
                    <option key={false} value={false}>
                      Bloqueados para não alunos da OQM
                    </option>
                  </TextFieldStyled>
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                      >
                        Salvar
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </PaperStyled>
    </Container>
  );
};

export default Item;
