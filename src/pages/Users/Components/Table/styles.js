import styled from 'styled-components';

export const Image = styled.img`
  width: 24px;
  height: 24px;
`;
