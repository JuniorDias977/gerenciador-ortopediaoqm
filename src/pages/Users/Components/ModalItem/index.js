/* eslint-disable func-names */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { format } from 'date-fns';
import emailjs, { init } from 'emailjs-com';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  MenuItem,
} from '@material-ui/core';

import firebase, { storage } from '~/services/firebase';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import { TextFieldStyled, ErrorMessageText, ImageStyled } from './styles';
import api from '~/services/api';

const sgMail = require('@sendgrid/mail');

const ModalItem = ({ handleCloseModal, open, item = null, getItens }) => {
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();
  const [imageLoadingThumb, setImageLoadingThumb] = useState();
  const [selectedFileThumb, setSelectedFileThumb] = useState();
  const [urlData, setUrlData] = useState();

  console.log(item, 'ITEM');

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o nome da Usuário'
    ),
    email: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o email da Usuário'
    ),
  });

  const levels = [
    {
      value: 'admin',
      label: 'Admin',
    },
    {
      value: 'studenty',
      label: 'Aluno',
    },
    {
      value: 'not_studenty',
      label: 'Não Aluno',
    },
  ];

  const handleSaveClasse = useCallback(
    async data => {
      sgMail.setApiKey(process.env.REACT_APP_SENDGRID_API_KEY);
      setLoading(true);

      if (item && item.key) {
        try {
          let image = '';

          console.log(data, 'DATA');

          if (selectedFileThumb) {
            const task = storage
              .ref(
                `files/----${item.key}----${format(
                  new Date(),
                  'yyyy-MM-dd'
                )}----${format(new Date(), 'HH:mm:ss')}`
              )
              .put(selectedFileThumb);

            const resp = await task;
            image = resp.metadata.fullPath;
          } else {
            image = item.avatar ? `${item.avatar}` : '';
          }

          const templateParams = {
            email: data.email,
          };

          init('user_viiWQcM1REPWvZSL9cfhQ');

          if (
            !item.active &&
            (data.active === 'true' || data.active === true)
          ) {
            emailjs.send(
              'service_sendgridortopedi',
              'template_c6hejxg',
              templateParams
            );
          } else if (
            item.active &&
            (data.active === 'false' || data.active === false)
          ) {
            emailjs.send(
              'service_sendgridortopedi',
              'template_8ekattj',
              templateParams
            );
          }

          // if (
          //   !item.is_istudenty &&
          //   (data.is_studenty || data.is_istudenty === 'true')
          // ) {
          //
          //   await api.post('/send', { email: data.email });
          // }

          firebase
            .firestore()
            .collection('users')
            .doc(item.key)
            .update({
              ...data,
              avatar: image,
              active: data.active === 'true' || data.active === true,
              is_studenty: !!(
                data.is_studenty === 'true' || data.is_istudenty === true
              ),
              is_admin: data.level === 'admin',
            })
            .then(() => {
              setLoading(false);
              addToast({
                type: 'success',
                title: language
                  ? 'Registered successfully'
                  : 'Salvo com sucesso',
              });
              handleCloseModal();
              getItens();
              setLoading(false);
            })
            .catch(function () {
              setLoading(false);
            });
        } catch (_) {
          setLoading(false);
        }
      } else {
        let image = '';

        if (selectedFileThumb) {
          const task = storage
            .ref(
              `files/----${format(new Date(), 'yyyy-MM-dd')}----${format(
                new Date(),
                'HH:mm:ss'
              )}`
            )
            .put(selectedFileThumb);
          const resp = await task;
          image = resp.metadata.fullPath;
        } else if (item) {
          image = item.avatar ? `${item.avatar}` : '';
        }

        firebase
          .auth()
          .createUserWithEmailAndPassword(data.email, data.password)
          .then(userCredential => {
            console.log(userCredential);
          })
          .catch(e => {
            console.log(e);
          });

        firebase
          .firestore()
          .collection('users')
          .add({
            ...data,
            avatar: image,
            date: format(new Date(), 'yyyy-MM-dd')
              .concat(' ')
              .concat(format(new Date(), 'HH:mm:ss')),
            active: true,
            is_admin: data.level === 'admin',
            is_studenty:
              !!(data.is_studenty === 'true' || data.is_istudenty === true) &&
              data.level === 'studenty',
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function (e) {
            console.log(e);
            addToast({
              type: 'error',
              title: 'Tente novamente',
            });
            setLoading(false);
          });
      }
    },
    [addToast, getItens, handleCloseModal, item, language, selectedFileThumb]
  );

  const fileSelectedHandler = useCallback(event => {
    const reader = new FileReader();
    if (event && event.target && event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
      setSelectedFileThumb(event.target.files[0]);

      reader.onload = e => {
        setImageLoadingThumb(e.target.result);
      };
    }
  }, []);

  useEffect(() => {
    async function getImage() {
      if (item && item.avatar) {
        storage
          .ref(item.avatar)
          .getDownloadURL()
          .then(url => {
            setUrlData(url);
            setLoading(false);
          })
          .catch(() => {
            setUrlData(item.avatar);
            setLoading(false);
          });
      }
    }
    getImage();
  }, [item]);

  return (
    <Dialog aria-labelledby="customized-dialog-title" open={open}>
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">{language ? 'User' : 'Usuário'} </Typography>
        </div>
      </DialogTitle>

      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            name: item && item.name ? item.name : '',
            email: item && item.email ? item.email : '',
            avatar: item && item.avatar ? item.avatar : '',
            password: '',
            active: item ? item.active : true,
            level: item && item.level ? item.level : 'studenty',
            residence: item && item.residence ? item.residence : '',
            is_studenty: item && item.is_studenty ? item.is_studenty : false,
          }}
          onSubmit={handleSaveClasse}
        >
          {({ values, handleChange, handleSubmit, errors, touched }) => (
            <Form>
              <Grid container>
                {!imageLoadingThumb && item && item.avatar && urlData ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Avatar</Typography>
                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={urlData} />
                    </div>
                  </Grid>
                ) : imageLoadingThumb ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Avatar</Typography>
                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={imageLoadingThumb} />
                    </div>
                  </Grid>
                ) : null}

                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    fullWidth
                    id="avatar"
                    name="avatar"
                    type="file"
                    required
                    onChange={fileSelectedHandler}
                    label="Avatar"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.avatar && touched.avatar ? (
                    <ErrorMessageText>{errors.avatar}</ErrorMessageText>
                  ) : null}
                </Grid>

                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.name}
                    fullWidth
                    id="name"
                    name="name"
                    required
                    onChange={handleChange}
                    label={language ? 'Name' : 'Nome'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.name && touched.name ? (
                    <ErrorMessageText>{errors.name}</ErrorMessageText>
                  ) : null}
                </Grid>

                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.email}
                    fullWidth
                    id="email"
                    name="email"
                    required
                    onChange={handleChange}
                    label="E-mail"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.email && touched.email ? (
                    <ErrorMessageText>{errors.email}</ErrorMessageText>
                  ) : null}
                </Grid>

                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.residence}
                    fullWidth
                    id="residence"
                    name="residence"
                    required
                    select
                    onChange={handleChange}
                    label={language ? 'Level user' : 'Ano de residência'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  >
                    <MenuItem key="R1" value="R1">
                      R1
                    </MenuItem>
                    <MenuItem key="R2" value="R2">
                      R2
                    </MenuItem>
                    <MenuItem key="R3" value="R3">
                      R3
                    </MenuItem>
                  </TextFieldStyled>
                  {errors.residence && touched.residence ? (
                    <ErrorMessageText>{errors.residence}</ErrorMessageText>
                  ) : null}
                </Grid>

                {item && !item.id_admin && (
                  <Grid item xs={12}>
                    <TextFieldStyled
                      value={values.is_studenty}
                      fullWidth
                      id="is_studenty"
                      name="is_studenty"
                      required
                      onChange={handleChange}
                      label="Aluno OQM?"
                      select
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      SelectProps={{
                        native: true,
                      }}
                    >
                      <option key={true} value={true}>
                        Sim
                      </option>

                      <option key={false} value={false}>
                        Não
                      </option>
                    </TextFieldStyled>
                  </Grid>
                )}

                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.password}
                    type="password"
                    fullWidth
                    id="password"
                    name="password"
                    required
                    onChange={handleChange}
                    label={language ? 'Password' : 'Senha'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.password && touched.password ? (
                    <ErrorMessageText>{errors.password}</ErrorMessageText>
                  ) : null}
                </Grid>

                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.level}
                    fullWidth
                    id="level"
                    name="level"
                    required
                    select
                    onChange={handleChange}
                    label={language ? 'Level user' : 'Nível'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  >
                    {levels && levels.length
                      ? levels.map(level => (
                          <MenuItem key={level.value} value={level.value}>
                            {level.label}
                          </MenuItem>
                        ))
                      : null}
                  </TextFieldStyled>
                  {errors.level && touched.level ? (
                    <ErrorMessageText>{errors.level}</ErrorMessageText>
                  ) : null}
                </Grid>
              </Grid>

              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={handleChange}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>

                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}

              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>

        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
};

export default ModalItem;
