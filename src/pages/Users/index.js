import React, { useEffect, useCallback, useState } from 'react';
import emailjs, { init } from 'emailjs-com';

import firebase from '~/services/firebase';
import { useToast } from '~/hooks/Toast';
import { useAuth } from '~/hooks/Auth';
import { useUsers } from '~/hooks/Users';
import Table from './Components/Table';
import ModalItem from './Components/ModalItem';

import { Container, PaperStyled, IconStyled, ButtonStyled } from './styles';
import history from '~/services/history';
import colors from '~/styles/colors';

const Item = () => {
  const { addToast } = useToast();

  const { users, getUsers } = useUsers();
  const { language, user } = useAuth();

  const [search, setSearch] = useState([]);
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();

  useEffect(() => {
    if (user && !user.is_admin) {
      addToast({
        type: 'error',
        title: 'Usuário sem permissão para acessar essa página.',
      });
      history.push('/dashboard');
    }
  }, [user, addToast]);

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  const handleChangeSearch = useCallback(value => {
    setSearch(value);
  }, []);

  const handleSearch = useCallback(() => {
    if (search !== '') {
      const filter = `${search}`;

      console.log(filter, 'FILTER');
      getUsers(filter);
    } else {
      const filter = '';
      getUsers(filter);
    }
  }, [search, getUsers]);

  const handleStatus = async (itemData, value) => {
    const obj = {
      ...itemData,
      active: value,
    };

    const templateParams = {
      email: itemData.email,
    };

    init('user_viiWQcM1REPWvZSL9cfhQ');

    if (!itemData.active && value) {
      emailjs.send(
        'service_sendgridortopedi',
        'template_c6hejxg',
        templateParams
      );
    } else if (itemData.active && !value) {
      emailjs.send(
        'service_sendgridortopedi',
        'template_8ekattj',
        templateParams
      );
    }

    try {
      firebase.firestore().collection('users').doc(itemData.key).update(obj);
      addToast({
        type: 'success',
        title: language ? 'Registered successfully' : 'Salvo com sucesso',
      });
      getUsers();
    } catch (err) {
      addToast({
        type: 'error',
        title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
      });
    }
  };

  const handleGetItem = useCallback(e => {
    if (e) setItem(e);
    setOpen(true);
  }, []);

  const handleCloseModal = useCallback(() => {
    setOpen(false);
    setItem(null);
  }, []);

  const handleSendMailRecover = useCallback(itemUser => {
    firebase
      .auth()
      .sendPasswordResetEmail(itemUser.email)
      .then(() => {
        addToast({
          type: 'success',
          title: language ? 'Send Email' : 'Email enviado',
        });
      })
      .catch(e => {
        console.log(e);
      });
  }, []);

  return (
    <Container>
      <PaperStyled>
        <ButtonStyled
          variant="contained"
          color="primary"
          style={{
            background: colors.orange,
          }}
          onClick={() => setOpen(!open)}
        >
          <IconStyled>add</IconStyled>
          {language ? 'Add user' : 'Novo usuário'}
        </ButtonStyled>

        <Table
          data={users}
          onChangeSearch={handleChangeSearch}
          search={handleSearch}
          alteredStatus={handleStatus}
          handleGetItem={handleGetItem}
          handleSendMailRecover={handleSendMailRecover}
        />
      </PaperStyled>

      <ModalItem
        handleCloseModal={handleCloseModal}
        open={open}
        getItens={getUsers}
        item={item}
      />
    </Container>
  );
};

export default Item;
