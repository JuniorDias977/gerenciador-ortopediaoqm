import React, { useState, useCallback } from 'react';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { Grid, TextField, InputAdornment, IconButton } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import {
  StyledContainer,
  StyledGridItem,
  StyledSubmitButton,
  StyledForm,
  IconStyled,
  Error,
  ErrorMessageText,
} from './styles';

import colors from '~/styles/colors';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import history from '~/services/history';

import logoImg from '~/assets/images/log.svg';

const SignIn = () => {
  const { signIn, loading, setLoading, language } = useAuth();
  const { addToast } = useToast();
  const [visiblePassword, setVisiblePassword] = useState(false);
  const [error, setError] = useState('');

  const schema = Yup.object().shape({
    email: Yup.string().required(language ? 'Enter email.' : 'Informe o email'),
    password: Yup.string().required(
      language ? 'Enter password' : 'Informe a senha'
    ),
  });

  const handleLogin = useCallback(
    async data => {
      try {
        await signIn(data, setError);
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
          description: language
            ? 'Check your email and password and try again'
            : 'Verifique seu email e senha e tente novamente',
        });
        setLoading(false);
      }
    },
    [signIn, addToast, setLoading, language]
  );

  return (
    <StyledContainer maxWidth="sm">
      <Formik
        validationSchema={schema}
        initialValues={{
          email: '',
          password: '',
        }}
        onSubmit={handleLogin}
      >
        {({ handleChange, handleSubmit, errors, touched }) => (
          <StyledForm>
            <img alt="Coupons" width="200" src={logoImg} />
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <StyledGridItem item sm marginTop={30}>
                <TextField
                  fullWidth
                  id="email"
                  name="email"
                  required
                  onChange={handleChange}
                  placeholder="Email"
                  variant="outlined"
                  style={{ backgroundColor: 'white', borderRadius: 8 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {errors.email && touched.email ? (
                  <ErrorMessageText>{errors.email}</ErrorMessageText>
                ) : null}
              </StyledGridItem>

              <StyledGridItem item sm>
                <TextField
                  id="password"
                  name="password"
                  variant="outlined"
                  fullWidth
                  disabled={false}
                  type={visiblePassword ? 'text' : 'password'}
                  placeholder="Senha"
                  // error={error}
                  onChange={handleChange}
                  style={{ backgroundColor: 'white', borderRadius: 8 }}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={() => setVisiblePassword(!visiblePassword)}
                          aria-label="toggle password visibility"
                          edge="end"
                        >
                          {visiblePassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),
                    'aria-label': 'Weight',
                    required: true,
                    startAdornment: (
                      <IconStyled fontSize="small" color="primary">
                        vpn_key
                      </IconStyled>
                    ),
                  }}
                />
                {errors.password && touched.password ? (
                  <ErrorMessageText>{errors.password}</ErrorMessageText>
                ) : null}
              </StyledGridItem>
              {error && <Error>{error}</Error>}

              <center>
                <StyledGridItem item marginTop={70}>
                  <StyledSubmitButton
                    variant="contained"
                    background={colors.blueButton}
                    color={colors.white}
                    type="submit"
                    onClick={handleSubmit}
                  >
                    {loading ? `${'Entrando...'}` : `${'Entrar'}`}
                  </StyledSubmitButton>
                </StyledGridItem>
                <StyledGridItem item marginTop={24}>
                  <StyledSubmitButton
                    variant="contained"
                    background={colors.blueButton}
                    color={colors.white}
                    type="submit"
                    onClick={() => history.push('/recover')}
                  >
                    Recuperar senha
                  </StyledSubmitButton>
                </StyledGridItem>

                {/* <StyledGridItem item>
                  <StyledSubmitButton
                    variant="outlined"
                    color="primary"
                    type="button"
                  >
                    Recuperar a senha
                  </StyledSubmitButton>
                </StyledGridItem> */}
              </center>
            </Grid>
          </StyledForm>
        )}
      </Formik>
    </StyledContainer>
  );
};

export default SignIn;
