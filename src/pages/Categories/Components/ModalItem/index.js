/* eslint-disable func-names */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { format } from 'date-fns';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core';

import firebase, { storage } from '~/services/firebase';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import { TextFieldStyled, ErrorMessageText, ImageStyled } from './styles';

const ModalItem = ({ handleCloseModal, open, item = null, getItens }) => {
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();
  const [imageLoadingThumb, setImageLoadingThumb] = useState();
  const [selectedFileThumb, setSelectedFileThumb] = useState();
  const [urlData, setUrlData] = useState();

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o título da categoria'
    ),
  });

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.key) {
        let image = '';
        if (selectedFileThumb) {
          const task = storage
            .ref(
              `files/----${item.key}----${format(
                new Date(),
                'yyyy-MM-dd'
              )}----${format(new Date(), 'HH:mm:ss')}`
            )
            .put(selectedFileThumb);
          const resp = await task;
          image = resp.metadata.fullPath;
        } else {
          image = item.icon ? `${item.icon}` : '';
        }
        firebase
          .firestore()
          .collection('categories')
          .doc(item.key)
          .update({
            ...data,
            icon: image,
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            setLoading(false);
          });
      } else {
        let image = '';
        if (selectedFileThumb) {
          const task = storage
            .ref(
              `files/----${format(new Date(), 'yyyy-MM-dd')}----${format(
                new Date(),
                'HH:mm:ss'
              )}`
            )
            .put(selectedFileThumb);
          const resp = await task;
          image = resp.metadata.fullPath;
        } else {
          image = item.icon ? `${item.icon}` : '';
        }
        firebase
          .firestore()
          .collection('categories')
          .add({
            ...data,
            icon: image,
            date: format(new Date(), 'yyyy-MM-dd')
              .concat(' ')
              .concat(format(new Date(), 'HH:mm:ss')),
            active: true,
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            addToast({
              type: 'error',
              title: 'Tente novamente',
            });
            setLoading(false);
          });
      }
    },
    [addToast, getItens, handleCloseModal, item, language, selectedFileThumb]
  );

  const fileSelectedHandler2 = useCallback(event => {
    const reader = new FileReader();
    if (event && event.target && event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
      setSelectedFileThumb(event.target.files[0]);

      reader.onload = e => {
        setImageLoadingThumb(e.target.result);
      };
    }
  }, []);

  useEffect(() => {
    async function getImage() {
      if (item && item.icon) {
        setLoading(true);
        storage
          .ref(item.icon)
          .getDownloadURL()
          .then(url => {
            setUrlData(url);
          });
        setLoading(false);
      }
    }
    getImage();
  }, [item]);

  return (
    <Dialog aria-labelledby="customized-dialog-title" open={open}>
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">
            {language ? 'Category' : 'Categoria'}{' '}
          </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            name: item && item.name ? item.name : '',
            icon: item && item.icon ? item.icon : '',
          }}
          onSubmit={handleSaveClasse}
        >
          {({ values, handleChange, handleSubmit, errors, touched }) => (
            <Form>
              <Grid container>
                {!imageLoadingThumb && item && item.icon && urlData ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Ícone</Typography>
                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={urlData} />
                    </div>
                  </Grid>
                ) : imageLoadingThumb ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Ícone</Typography>
                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={imageLoadingThumb} />
                    </div>
                  </Grid>
                ) : null}
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    fullWidth
                    id="icon"
                    name="icon"
                    type="file"
                    required
                    onChange={fileSelectedHandler2}
                    label="Ícone"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.icon && touched.icon ? (
                    <ErrorMessageText>{errors.icon}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.name}
                    fullWidth
                    id="name"
                    name="name"
                    required
                    onChange={handleChange}
                    label={language ? 'Name' : 'Título da categoria'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.name && touched.name ? (
                    <ErrorMessageText>{errors.name}</ErrorMessageText>
                  ) : null}
                </Grid>
              </Grid>
              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={handleChange}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
};

export default ModalItem;
