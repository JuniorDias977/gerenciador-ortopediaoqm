import React, { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';

import firebase from '~/services/firebase';
import { useToast } from '~/hooks/Toast';
import { useAuth } from '~/hooks/Auth';
import { useSubCategories } from '~/hooks/SubCategories';
import Table from './Components/Table';
import ModalItem from './Components/ModalItem';

import { Container, PaperStyled, IconStyled, ButtonStyled } from './styles';
import history from '~/services/history';
import colors from '~/styles/colors';

const Item = ({ match }) => {
  const { addToast } = useToast();
  const { subCategories, getSubCategories } = useSubCategories();
  const { language, user } = useAuth();
  const [search, setSearch] = useState([]);
  const [subCategoriesData, setSubCategoriesData] = useState([]);
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();
  const { id } = match.params;

  const handleChangeSearch = useCallback(value => {
    setSearch(value);
  }, []);

  const handleSearch = useCallback(() => {
    if (search !== '') {
      const filter = `${search}`;
      getSubCategories(filter);
    } else {
      const filter = '';
      getSubCategories(filter);
    }
  }, [search, getSubCategories]);

  const handleStatus = useCallback(
    async (itemData, value) => {
      const obj = {
        ...itemData,
        active: value,
      };
      try {
        firebase
          .firestore()
          .collection('sub_categories')
          .doc(itemData.key)
          .update(obj);
        addToast({
          type: 'success',
          title: language ? 'Registered successfully' : 'Salvo com sucesso',
        });
        getSubCategories();
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    },
    [addToast, getSubCategories, language]
  );

  useEffect(() => {
    getSubCategories();
  }, [getSubCategories]);

  const handleGetItem = useCallback(e => {
    if (e) setItem(e);
    setOpen(true);
  }, []);

  const handleCloseModal = useCallback(() => {
    setOpen(false);
    setItem(null);
  }, []);

  useEffect(() => {
    if (user && !user.is_admin) {
      addToast({
        type: 'error',
        title: 'Usuário sem permissão para acessar essa página.',
      });
      history.push('/dashboard');
    }
  }, [user, addToast]);

  useEffect(() => {
    if (subCategories && subCategories.length) {
      // eslint-disable-next-line prefer-const
      let newCategories = [];
      subCategories.forEach(category => {
        if (
          !newCategories.find(ctgry => ctgry.key === category.key) &&
          category.category.value === id
        ) {
          newCategories.push(category);
        }
      });
      setSubCategoriesData(newCategories);
    } else {
      setSubCategoriesData([]);
    }
  }, [subCategories, id]);

  return (
    <Container>
      <PaperStyled>
        <ButtonStyled
          variant="contained"
          color="primary"
          style={{
            background: colors.orange,
          }}
          onClick={() => setOpen(!open)}
        >
          <IconStyled>add</IconStyled>
          {language ? 'Add sub category' : 'Nova sub categoria'}
        </ButtonStyled>
        <Table
          data={subCategoriesData}
          onChangeSearch={handleChangeSearch}
          search={handleSearch}
          alteredStatus={handleStatus}
          handleGetItem={handleGetItem}
        />
      </PaperStyled>
      <ModalItem
        handleCloseModal={handleCloseModal}
        open={open}
        categoryId={id}
        getItens={getSubCategories}
        item={item}
      />
    </Container>
  );
};

Item.propTypes = {
  match: PropTypes.node.isRequired,
};

export default Item;
