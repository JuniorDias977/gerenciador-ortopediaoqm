/* eslint-disable array-callback-return */
/* eslint-disable func-names */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import Select from 'react-select';
import { format } from 'date-fns';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Checkbox,
} from '@material-ui/core';

import firebase, { storage } from '~/services/firebase';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';
import { useSubGroups } from '~/hooks/SubGroups';
import { useCategories } from '~/hooks/Categories';
import { useSubCategories } from '~/hooks/SubCategories';
import { useTags } from '~/hooks/Tags';

import { TextFieldStyled, ErrorMessageText, ImageStyled } from './styles';

const ModalItem = ({ handleCloseModal, open, item = null, getItens }) => {
  const { subGroups, getSubGroups } = useSubGroups();
  const { categories, getCategories } = useCategories();
  const { subCategories, getSubCategories } = useSubCategories();

  const { tags, getTags } = useTags();
  const { addToast } = useToast();

  const [loading, setLoading] = useState(false);
  const { language } = useAuth();

  const [subGroupData, setSubGroupData] = useState([]);
  const [subCategoryData, setSubCategoryData] = useState([]);
  const [categoryData, setCategoryData] = useState([]);

  const [tagData, setTagData] = useState([]);

  const [categorySelected, setCategorySelected] = useState('');
  const [subCategorySelected, setSubCategorySelected] = useState('');

  const [imageLoadingThumb, setImageLoadingThumb] = useState();
  const [selectedFileThumb, setSelectedFileThumb] = useState();
  const [urlData, setUrlData] = useState();

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);

      if (item && item.key) {
        let image = '';

        if (selectedFileThumb) {
          const task = storage
            .ref(
              `files/----${format(new Date(), 'yyyy-MM-dd')}----${format(
                new Date(),
                'HH:mm:ss'
              )}`
            )
            .put(selectedFileThumb);

          const resp = await task;
          image = resp.metadata.fullPath;
        } else if (item) {
          image = item.avatar ? `${item.avatar}` : '';
        }

        firebase
          .firestore()
          .collection('questions')
          .doc(item.key)
          .update({
            ...data,
            image,
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            setLoading(false);
          });
      } else {
        let image = '';

        if (selectedFileThumb) {
          const task = storage
            .ref(
              `files/----${format(new Date(), 'yyyy-MM-dd')}----${format(
                new Date(),
                'HH:mm:ss'
              )}`
            )
            .put(selectedFileThumb);
          const resp = await task;
          image = resp.metadata.fullPath;
        } else if (item) {
          image = item.avatar ? `${item.avatar}` : '';
        }

        firebase
          .firestore()
          .collection('questions')
          .add({
            ...data,
            image,
            date: format(new Date(), 'yyyy-MM-dd')
              .concat(' ')
              .concat(format(new Date(), 'HH:mm:ss')),
            active: true,
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            addToast({
              type: 'error',
              title: 'Tente novamente',
            });
            setLoading(false);
          });
      }
    },
    [addToast, getItens, handleCloseModal, item, language]
  );

  useEffect(() => {
    getSubGroups();
  }, [getSubGroups]);

  useEffect(() => {
    getTags();
  }, [getTags]);

  useEffect(() => {
    getCategories();
  }, [getCategories]);

  useEffect(() => {
    getSubCategories();
  }, [getSubCategories]);

  useEffect(() => {
    async function getSubCategoriesData() {
      try {
        setLoading(true);
        const arr = [];

        subCategories.map(category => {
          if (category.active && categorySelected) {
            const objData = {
              value: category.key,
              label: category.name,
            };

            if (categorySelected) {
              if (category.category.value === categorySelected) {
                arr.push(objData);
              }
              return;
            }
            arr.push(objData);
          }
        });

        setSubCategoryData(arr);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    }
    getSubCategoriesData();
  }, [addToast, language, subCategories, categorySelected]);

  useEffect(() => {
    async function getCategoriesData() {
      try {
        setLoading(true);
        const arr = [];
        categories.map(category => {
          if (category.active) {
            const objData = {
              value: category.key,
              label: category.name,
            };
            arr.push(objData);
          }
        });
        setCategoryData(arr);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    }
    getCategoriesData();
  }, [addToast, language, categories]);

  useEffect(() => {
    async function getCategoriesData() {
      try {
        setLoading(true);
        const arr = [];
        subGroups.map(category => {
          if (category.active && subCategorySelected) {
            const objData = {
              value: category.key,
              label: category.name,
            };
            if (subCategorySelected) {
              if (category.sub_category.value === subCategorySelected) {
                arr.push(objData);
              }
              return;
            }
            arr.push(objData);
          }
        });
        setSubGroupData(arr);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    }
    getCategoriesData();
  }, [addToast, language, subGroups, subCategorySelected]);

  useEffect(() => {
    async function getCategoriesData() {
      try {
        setLoading(true);
        const arr = [];
        tags.map(tag => {
          if (tag.active) {
            const objData = {
              value: tag.key,
              label: tag.name,
            };
            arr.push(objData);
          }
        });
        setTagData(arr);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    }
    getCategoriesData();
  }, [addToast, language, tags]);

  useEffect(() => {
    async function getImage() {
      if (item && item.image) {
        storage
          .ref(item.image)
          .getDownloadURL()
          .then(url => {
            setUrlData(url);
            setLoading(false);
          })
          .catch(() => {
            setUrlData(item.image);
            setLoading(false);
          });
      }
    }
    getImage();
  }, [item]);

  const fileSelectedHandler = useCallback(event => {
    const reader = new FileReader();

    if (event && event.target && event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
      setSelectedFileThumb(event.target.files[0]);

      reader.onload = e => {
        setImageLoadingThumb(e.target.result);
      };
    }
  }, []);

  return (
    <Dialog aria-labelledby="customized-dialog-title" open={open}>
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">
            {language ? 'Question' : 'Questão'}{' '}
          </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          initialValues={{
            image: item && item.image ? item.image : '',
            sub_group: item && item.sub_group ? item.sub_group : '',
            name: item && item.name ? item.name : '',
            video: item && item.video ? item.video : '',
            lesson_video: item && item.lesson_video ? item.lesson_video : '',
            description: item && item.description ? item.description : '',
            is_video: item && item.is_video ? item.is_video : false,
            text: item && item.text ? item.text : false,
            alternative_a: item && item.alternative_a ? item.alternative_a : '',
            is_correct_a: item && item.is_correct_a ? item.is_correct_a : false,
            alternative_b: item && item.alternative_b ? item.alternative_b : '',
            is_correct_b: item && item.is_correct_b ? item.is_correct_b : false,
            alternative_c: item && item.alternative_c ? item.alternative_c : '',
            is_correct_c: item && item.is_correct_c ? item.is_correct_c : false,
            alternative_d: item && item.alternative_d ? item.alternative_d : '',
            is_correct_d: item && item.is_correct_d ? item.is_correct_d : false,
            tags: item && item.tags ? item.tags : [],
            description_answer:
              item && item.description_answer ? item.description_answer : '',
            category: item && item.category ? item.category : '',
            sub_category: item && item.sub_category ? item.sub_category : '',
          }}
          onSubmit={handleSaveClasse}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            errors,
            touched,
            setFieldValue,
          }) => (
            <Form>
              <Grid item xs={12} sm={12} md={12}>
                {item && item.image && (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Imagem</Typography>

                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={urlData} />
                    </div>
                  </Grid>
                )}

                <TextFieldStyled
                  fullWidth
                  id="image"
                  name="image"
                  type="file"
                  required
                  onChange={fileSelectedHandler}
                  label="Imagem"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {errors.image && touched.image ? (
                  <ErrorMessageText>{errors.image}</ErrorMessageText>
                ) : null}
              </Grid>

              <Grid item xs={12} sm={12} md={12}>
                <TextFieldStyled
                  fullWidth
                  id="name"
                  name="name"
                  required
                  value={values.name}
                  onChange={handleChange}
                  label="Título"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {errors.name && touched.name ? (
                  <ErrorMessageText>{errors.name}</ErrorMessageText>
                ) : null}
              </Grid>
              <Grid container>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 8, zIndex: 1004 }}
                >
                  <span>Selecione a categoria</span>
                  <Select
                    options={categoryData}
                    value={values.category}
                    placeholder="Selecione a categoria"
                    style={{ background: 'white', zIndex: 1003 }}
                    label="Selecione a categoria"
                    onChange={e => {
                      setFieldValue('category', e);
                      setCategorySelected(e.value);
                    }}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 8, zIndex: 1002 }}
                >
                  <span>Selecione a sub categoria</span>
                  <Select
                    options={subCategoryData}
                    value={values.sub_category}
                    placeholder="Selecione a sub categoria"
                    style={{ background: 'white', zIndex: 1001 }}
                    label="Selecione a sub categoria"
                    onChange={e => {
                      setFieldValue('sub_category', e);
                      setSubCategorySelected(e.value);
                    }}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 8, zIndex: 1000 }}
                >
                  <span>Selecione o sub grupo</span>
                  <Select
                    options={subGroupData}
                    value={values.sub_group}
                    isMulti
                    placeholder="Selecione o sub grupo"
                    style={{ background: 'white', zIndex: 999 }}
                    label="Selecione o sub grupo"
                    onChange={e => {
                      setFieldValue('sub_group', e);
                    }}
                  />
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 8, zIndex: 998 }}
                >
                  <span>Selecione as tags</span>
                  <Select
                    options={tagData}
                    value={values.tags}
                    isMulti
                    placeholder="Selecione as tags"
                    style={{ background: 'white', zIndex: 997 }}
                    label="Selecione as tags"
                    onChange={e => {
                      setFieldValue('tags', e);
                    }}
                  />
                </Grid>
                <h4 style={{ paddingLeft: 8, paddingTop: 16 }}>Pergunta</h4>
                <Grid item xs={12} sm={12} md={12}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Checkbox
                        checked={values.is_video}
                        color="primary"
                        onChange={() => {
                          setFieldValue('is_video', !values.is_video);
                          setFieldValue('text', false);
                        }}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                      />
                      <Typography>Vídeo</Typography>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                      <Checkbox
                        checked={!item ? true : values.text}
                        color="primary"
                        onChange={() => {
                          setFieldValue('text', !values.text);
                          setFieldValue('is_video', false);
                        }}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                      />
                      <Typography>Texto</Typography>
                    </div>
                  </div>
                </Grid>
                {values.is_video ? (
                  <>
                    <Grid item xs={12} sm={12} md={12}>
                      <TextFieldStyled
                        fullWidth
                        id="video"
                        name="video"
                        required
                        value={values.video}
                        onChange={handleChange}
                        label="Questão em vídeo"
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.video && touched.video ? (
                        <ErrorMessageText>{errors.video}</ErrorMessageText>
                      ) : null}
                    </Grid>
                  </>
                ) : (
                  <>
                    <Grid
                      item
                      xs={12}
                      sm={12}
                      md={12}
                      style={{ minWidth: 500 }}
                    >
                      <TextFieldStyled
                        value={values.description}
                        fullWidth
                        id="description"
                        name="description"
                        required
                        rows={4}
                        multiline
                        onChange={handleChange}
                        label={language ? 'Name' : 'Questão em texto'}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                      {errors.description && touched.description ? (
                        <ErrorMessageText>
                          {errors.description}
                        </ErrorMessageText>
                      ) : null}
                    </Grid>
                  </>
                )}

                <h4 style={{ paddingLeft: 8, paddingTop: 16 }}>Respostas</h4>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_a}
                      fullWidth
                      id="alternative_a"
                      name="alternative_a"
                      required
                      onChange={handleChange}
                      label="Alternativa A"
                      style={{ marginBottom: 0 }}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_a && touched.alternative_a ? (
                      <ErrorMessageText>
                        {errors.alternative_a}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_b}
                      fullWidth
                      id="alternative_b"
                      name="alternative_b"
                      required
                      onChange={handleChange}
                      label="Alternativa B"
                      variant="outlined"
                      style={{ marginBottom: 0 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_b && touched.alternative_b ? (
                      <ErrorMessageText>
                        {errors.alternative_b}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_c}
                      fullWidth
                      id="alternative_c"
                      name="alternative_c"
                      required
                      onChange={handleChange}
                      label="Alternativa C"
                      variant="outlined"
                      style={{ marginBottom: 0 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_c && touched.alternative_c ? (
                      <ErrorMessageText>
                        {errors.alternative_c}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_d}
                      fullWidth
                      id="alternative_d"
                      name="alternative_d"
                      required
                      onChange={handleChange}
                      label="Alternativa D"
                      variant="outlined"
                      style={{ marginBottom: 0 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_d && touched.alternative_d ? (
                      <ErrorMessageText>
                        {errors.alternative_d}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={12} sm={12} md={12} style={{ paddingLeft: 8 }}>
                  <span>Resposta Correta</span>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_a}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_a', !values.is_correct_a);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>A</Typography>
                  </div>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_b}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_b', !values.is_correct_b);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>B</Typography>
                  </div>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_c}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_c', !values.is_correct_c);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>C</Typography>
                  </div>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_d}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_d', !values.is_correct_d);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>D</Typography>
                  </div>
                </Grid>
              </Grid>

              <h4 style={{ paddingLeft: 8, paddingTop: 16 }}>Explicação</h4>
              <Grid item xs={12} sm={12} md={12}>
                <TextFieldStyled
                  fullWidth
                  value={values.lesson_video}
                  id="lesson_video"
                  name="lesson_video"
                  onChange={handleChange}
                  label="Explicação em vídeo"
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {errors.lesson_video && touched.lesson_video ? (
                  <ErrorMessageText>{errors.lesson_video}</ErrorMessageText>
                ) : null}
              </Grid>
              <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                <TextFieldStyled
                  value={values.description_answer}
                  fullWidth
                  id="description_answer"
                  name="description_answer"
                  rows={4}
                  multiline
                  onChange={handleChange}
                  label={language ? 'Name' : 'Explicação em texto'}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {errors.description_answer && touched.description_answer ? (
                  <ErrorMessageText>
                    {errors.description_answer}
                  </ErrorMessageText>
                ) : null}
              </Grid>
              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={handleChange}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
};

export default ModalItem;
