/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-async-promise-executor */
import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { Icon, Grid, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';

import MaterialTable from 'material-table';
import { useAuth } from '~/hooks/Auth';

const Table = ({ alteredStatus, data, handleGetItem }) => {
  const { language } = useAuth();

  const handleChangeStatus = useCallback(
    async (secureId, type) => {
      alteredStatus(secureId, type);
    },
    [alteredStatus]
  );

  const columns = [
    {
      field: 'name',
      title: language ? 'Name' : 'Nome',
      filtering: true,
      sort: true,
      cellStyle: {
        width: '30%',
        minWidth: '30%',
      },
    },
    {
      field: 'active',
      title: 'Status',
      cellStyle: {
        width: '5%',
        minWidth: '5%',
      },
      filtering: false,
      sort: true,
      render: rowData => (
        <div>
          {rowData.active === true
            ? `${language ? 'Active' : 'Ativa'}`
            : `${language ? 'Inative' : 'Inativa'}`}
        </div>
      ),
    },
    {
      field: 'actions',
      title: language ? 'Actions' : 'Ações',
      filtering: false,
      sort: false,
      cellStyle: {
        width: '15%',
        minWidth: '15%',
      },
      render: rowData => (
        <Grid container>
          <Grid item>
            <IconButton
              color="primary"
              variant="contained"
              size="small"
              onClick={() => {
                handleGetItem(rowData);
              }}
            >
              <Icon>edit</Icon>
            </IconButton>
          </Grid>
          <Grid item>
            {rowData.active !== false ? (
              <IconButton
                aria-label="delete"
                style={{ color: '#ff0059' }}
                color="#ff0059"
                size="small"
                onClick={() => handleChangeStatus(rowData, false)}
              >
                <DeleteIcon />
              </IconButton>
            ) : (
              <IconButton
                color="primary"
                variant="contained"
                size="small"
                onClick={() => {
                  handleChangeStatus(rowData, true);
                }}
              >
                <Icon>check</Icon>
              </IconButton>
            )}
          </Grid>
        </Grid>
      ),
    },
  ];

  const options = {
    print: false,
    download: false,
    viewColumns: false,
    search: false,
    filtering: true,
    actionsColumnIndex: -1,
    filterType: 'checkbox',
    responsive: 'scroll',
    pageSize: 5,
    page: 0,
  };

  return (
    <div>
      <MaterialTable
        title=""
        data={data}
        columns={columns}
        options={options}
        localization={{
          pagination: {
            labelDisplayedRows: '{from}-{to} de {count}',
            labelRowsSelect: '',
            labelRowsPerPage: '',
            firstTooltip: '',
            previousTooltip: '',
            nextTooltip: '',
            lastTooltip: '',
          },
        }}
      />
    </div>
  );
};

Table.propTypes = {
  alteredStatus: PropTypes.func.isRequired,
  data: PropTypes.shape().isRequired,
  handleGetItem: PropTypes.func.isRequired,
};

export default Table;
