/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import InputMask from 'react-input-mask';
import { format, parseISO } from 'date-fns';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  MenuItem,
} from '@material-ui/core';

import api from '~/services/api';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';

import { TextFieldStyled, ErrorMessageText, ImageStyled } from './styles';

const ModalItem = ({ handleCloseModal, open, item = null, getItens }) => {
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();
  const [imageLoadingThumb, setImageLoadingThumb] = useState();
  const [selectedFileThumb, setSelectedFileThumb] = useState();

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o nome do Usuário'
    ),
    email: Yup.string().required(
      language ? 'Enter the name of the user.' : 'Informe o email do Usuário'
    ),
  });

  const levels = [
    {
      value: false,
      label: language ? 'Client' : 'Cliente',
    },
  ];

  const uploadImage = useCallback(async (file, id) => {
    const upImage = await api
      .patch(`users/${id}/avatar`, file, {
        // onUploadProgress: progressEvent => {}
      })
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return error;
      });
    return upImage;
  }, []);

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.id) {
        api
          .put(`users/${item.id}`, {
            ...data,
          })
          .then(async () => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
            if (selectedFileThumb) {
              const fd = new FormData();
              fd.append('avatar', selectedFileThumb, selectedFileThumb.name);
              await uploadImage(fd, item.id);
            }
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      } else {
        api
          .post(`users`, {
            ...data,
          })
          .then(async resp => {
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);

            if (selectedFileThumb) {
              const fd = new FormData();
              fd.append('avatar', selectedFileThumb, selectedFileThumb.name);
              await uploadImage(fd, resp.data.id);
            }
          })
          .catch(() => {
            addToast({
              type: 'error',
              title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
            });
            setLoading(false);
          });
      }
    },
    [
      addToast,
      getItens,
      handleCloseModal,
      item,
      language,
      selectedFileThumb,
      uploadImage,
    ]
  );

  const fileSelectedHandler2 = useCallback(event => {
    const reader = new FileReader();
    if (event && event.target && event.target.files[0]) {
      reader.readAsDataURL(event.target.files[0]);
      setSelectedFileThumb(event.target.files[0]);

      reader.onload = e => {
        setImageLoadingThumb(e.target.result);
      };
    }
  }, []);

  return (
    <Dialog
      // onClose={this.props.handleModalItem}
      aria-labelledby="customized-dialog-title"
      open={open}
    >
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">
            {language ? 'Client' : 'Cliente'}{' '}
          </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            name: item && item.name ? item.name : '',
            email: item && item.email ? item.email : '',
            avatar: item && item.avatar_url ? item.avatar_url : '',
            cpf: item && item.cpf ? item.cpf : '',
            password: '',
            active: item ? item.active : true,
            is_admin: item && item.is_admin ? item.is_admin : false,
            birth_date:
              item && item.birth_date
                ? format(parseISO(item.birth_date), 'yyyy-MM-dd')
                : format(new Date(), 'yyyy-MM-dd'),
          }}
          onSubmit={handleSaveClasse}
        >
          {({ values, handleChange, handleSubmit, errors, touched }) => (
            <Form>
              <Grid container>
                {!imageLoadingThumb && item && item.avatar_url ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Avatar</Typography>
                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={item.avatar_url} />
                    </div>
                  </Grid>
                ) : imageLoadingThumb ? (
                  <Grid item xs={12} sm={12} md={12}>
                    <Typography>Avatar</Typography>
                    <div style={{ marginTop: 8 }}>
                      <ImageStyled alt="thumb" src={imageLoadingThumb} />
                    </div>
                  </Grid>
                ) : null}
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    fullWidth
                    id="avatar_url"
                    name="avatar_url"
                    type="file"
                    required
                    onChange={fileSelectedHandler2}
                    label="Avatar"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.avatar_url && touched.avatar_url ? (
                    <ErrorMessageText>{errors.avatar_url}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.name}
                    fullWidth
                    id="name"
                    name="name"
                    required
                    onChange={handleChange}
                    label={language ? 'Name' : 'Nome'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.name && touched.name ? (
                    <ErrorMessageText>{errors.name}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.email}
                    fullWidth
                    id="email"
                    name="email"
                    required
                    onChange={handleChange}
                    label="E-mail"
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.email && touched.email ? (
                    <ErrorMessageText>{errors.email}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <InputMask
                    mask="999.999.999-99"
                    fullWidth
                    value={values.cpf}
                    name="cpf"
                    label={language ? 'Document' : 'CPF'}
                    onChange={handleChange}
                  >
                    {inputProps => (
                      <TextFieldStyled
                        {...inputProps}
                        fullWidth
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    )}
                  </InputMask>
                  {errors.cpf && touched.cpf ? (
                    <ErrorMessageText>{errors.cpf}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.birth_date}
                    fullWidth
                    id="birth_date"
                    name="birth_date"
                    onChange={handleChange}
                    type="date"
                    label={language ? 'Birth date' : 'Data de nascimento'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.birth_date && touched.birth_date ? (
                    <ErrorMessageText>{errors.birth_date}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.password}
                    type="password"
                    fullWidth
                    id="password"
                    name="password"
                    required
                    onChange={handleChange}
                    label={language ? 'Password' : 'Senha'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.password && touched.password ? (
                    <ErrorMessageText>{errors.password}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TextFieldStyled
                    value={values.is_admin}
                    fullWidth
                    id="is_admin"
                    name="is_admin"
                    required
                    select
                    onChange={handleChange}
                    label={language ? 'Level user' : 'Nível de permissão'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  >
                    {levels && levels.length
                      ? levels.map(level => (
                          <MenuItem key={level.value} value={level.value}>
                            {level.label}
                          </MenuItem>
                        ))
                      : null}
                  </TextFieldStyled>
                  {errors.is_admin && touched.is_admin ? (
                    <ErrorMessageText>{errors.is_admin}</ErrorMessageText>
                  ) : null}
                </Grid>
              </Grid>
              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={handleChange}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
};

export default ModalItem;
