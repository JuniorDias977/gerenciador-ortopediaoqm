import React, { useEffect, useCallback, useState } from 'react';

import firebase from '~/services/firebase';
import { useToast } from '~/hooks/Toast';
import { useAuth } from '~/hooks/Auth';
import { useTags } from '~/hooks/Tags';
import Table from './Components/Table';
import ModalItem from './Components/ModalItem';

import { Container, PaperStyled, IconStyled, ButtonStyled } from './styles';
import history from '~/services/history';
import colors from '~/styles/colors';

const Item = () => {
  const { addToast } = useToast();
  const { tags, getTags } = useTags();
  const { language, user } = useAuth();
  const [search, setSearch] = useState([]);
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();

  const handleChangeSearch = useCallback(value => {
    setSearch(value);
  }, []);

  const handleSearch = useCallback(() => {
    if (search !== '') {
      const filter = `${search}`;
      getTags(filter);
    } else {
      const filter = '';
      getTags(filter);
    }
  }, [search, getTags]);

  const handleStatus = useCallback(
    async (itemData, value) => {
      const obj = {
        ...itemData,
        active: value,
      };
      try {
        firebase
          .firestore()
          .collection('sub_groups')
          .doc(itemData.key)
          .update(obj);
        addToast({
          type: 'success',
          title: language ? 'Registered successfully' : 'Salvo com sucesso',
        });
        getTags();
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    },
    [addToast, getTags, language]
  );

  useEffect(() => {
    getTags();
  }, [getTags]);

  const handleGetItem = useCallback(e => {
    if (e) setItem(e);
    setOpen(true);
  }, []);

  const handleCloseModal = useCallback(() => {
    setOpen(false);
    setItem(null);
  }, []);

  useEffect(() => {
    if (user && !user.is_admin) {
      addToast({
        type: 'error',
        title: 'Usuário sem permissão para acessar essa página.',
      });
      history.push('/dashboard');
    }
  }, [user, addToast]);

  return (
    <Container>
      <PaperStyled>
        <ButtonStyled
          variant="contained"
          color="primary"
          style={{
            background: colors.orange,
          }}
          onClick={() => setOpen(!open)}
        >
          <IconStyled>add</IconStyled>
          {language ? 'Add tag' : 'Nova tag'}
        </ButtonStyled>
        <Table
          data={tags}
          onChangeSearch={handleChangeSearch}
          search={handleSearch}
          alteredStatus={handleStatus}
          handleGetItem={handleGetItem}
        />
      </PaperStyled>
      <ModalItem
        handleCloseModal={handleCloseModal}
        open={open}
        getItens={getTags}
        item={item}
      />
    </Container>
  );
};

export default Item;
