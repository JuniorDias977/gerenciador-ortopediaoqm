import React, { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';

import firebase from '~/services/firebase';
import { useToast } from '~/hooks/Toast';
import { useAuth } from '~/hooks/Auth';
import { useQuestionsSimulate } from '~/hooks/QuestionsSimulate';
import Table from './Components/Table';
import ModalItem from './Components/ModalItem';

import { Container, PaperStyled, IconStyled, ButtonStyled } from './styles';
import history from '~/services/history';
import colors from '~/styles/colors';

const Item = ({ match }) => {
  const { addToast } = useToast();
  const { questionsSimulate, getQuestionsSimulate } = useQuestionsSimulate();
  const [uestionsSimulateData, setQuestionsSimulateData] = useState([]);
  const { language, user } = useAuth();
  const [search, setSearch] = useState([]);
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();
  const { id } = match.params;

  const handleChangeSearch = useCallback(value => {
    setSearch(value);
  }, []);

  const handleSearch = useCallback(() => {
    if (search !== '') {
      const filter = `${search}`;
      getQuestionsSimulate(filter);
    } else {
      const filter = '';
      getQuestionsSimulate(filter);
    }
  }, [search, getQuestionsSimulate]);

  const handleStatus = useCallback(
    async (itemData, value) => {
      const obj = {
        ...itemData,
        active: value,
      };
      try {
        firebase
          .firestore()
          .collection('questions_simulate')
          .doc(itemData.key)
          .update(obj);
        addToast({
          type: 'success',
          title: language ? 'Registered successfully' : 'Salvo com sucesso',
        });
        getQuestionsSimulate();
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    },
    [addToast, getQuestionsSimulate, language]
  );

  useEffect(() => {
    getQuestionsSimulate();
  }, [getQuestionsSimulate]);

  const handleGetItem = useCallback(e => {
    if (e) setItem(e);
    setOpen(true);
  }, []);

  const handleCloseModal = useCallback(() => {
    setOpen(false);
    setItem(null);
  }, []);

  useEffect(() => {
    if (user && !user.is_admin) {
      addToast({
        type: 'error',
        title: 'Usuário sem permissão para acessar essa página.',
      });
      history.push('/dashboard');
    }
  }, [user, addToast]);

  useEffect(() => {
    if (questionsSimulate && questionsSimulate.length) {
      // eslint-disable-next-line prefer-const
      let newQuestionSimulate = [];
      questionsSimulate.forEach(category => {
        if (
          !newQuestionSimulate.find(ctgry => ctgry.key === category.key) &&
          category.simulate.value === id
        ) {
          newQuestionSimulate.push(category);
        }
      });
      setQuestionsSimulateData(newQuestionSimulate);
    } else {
      setQuestionsSimulateData([]);
    }
  }, [questionsSimulate, id]);

  return (
    <Container>
      <PaperStyled>
        <ButtonStyled
          variant="contained"
          color="primary"
          style={{
            background: colors.orange,
          }}
          onClick={() => setOpen(!open)}
        >
          <IconStyled>add</IconStyled>
          {language ? 'Add sub group' : 'Nova questão'}
        </ButtonStyled>
        <Table
          data={uestionsSimulateData || []}
          onChangeSearch={handleChangeSearch}
          search={handleSearch}
          alteredStatus={handleStatus}
          handleGetItem={handleGetItem}
        />
      </PaperStyled>
      <ModalItem
        handleCloseModal={handleCloseModal}
        open={open}
        getItens={getQuestionsSimulate}
        categoryId={id}
        item={item}
      />
    </Container>
  );
};

Item.propTypes = {
  match: PropTypes.node.isRequired,
};

export default Item;
