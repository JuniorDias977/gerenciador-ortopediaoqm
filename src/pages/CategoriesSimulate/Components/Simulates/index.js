import React, { useEffect, useCallback, useState } from 'react';
import PropTypes from 'prop-types';

import firebase from '~/services/firebase';
import { useToast } from '~/hooks/Toast';
import { useAuth } from '~/hooks/Auth';
import { useSimulate } from '~/hooks/Simulate';
import Table from './Components/Table';
import ModalItem from './Components/ModalItem';

import { Container, PaperStyled, IconStyled, ButtonStyled } from './styles';
import history from '~/services/history';
import colors from '~/styles/colors';

const Item = ({ match }) => {
  const { addToast } = useToast();

  const { simulates, getSimulate } = useSimulate();
  const { language, user } = useAuth();

  const [search, setSearch] = useState([]);
  const [simulateData, setSimulateData] = useState([]);

  const [open, setOpen] = useState(false);
  const [item, setItem] = useState();

  const { id } = match.params;

  const handleChangeSearch = useCallback(value => {
    setSearch(value);
  }, []);

  const handleSearch = useCallback(() => {
    if (search !== '') {
      const filter = `${search}`;
      getSimulate(filter);
    } else {
      const filter = '';
      getSimulate(filter);
    }
  }, [search, getSimulate]);

  const handleStatus = useCallback(
    async (itemData, value) => {
      const obj = {
        ...itemData,
        active: value,
      };
      try {
        firebase
          .firestore()
          .collection('simulates')
          .doc(itemData.key)
          .update(obj);
        addToast({
          type: 'success',
          title: language ? 'Registered successfully' : 'Salvo com sucesso',
        });
        getSimulate();
      } catch (err) {
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    },
    [addToast, getSimulate, language]
  );

  useEffect(() => {
    getSimulate();
  }, [getSimulate]);

  const handleGetItem = useCallback(e => {
    if (e) setItem(e);
    setOpen(true);
  }, []);

  const handleCloseModal = useCallback(() => {
    setOpen(false);
    setItem(null);
  }, []);

  useEffect(() => {
    if (user && !user.is_admin) {
      addToast({
        type: 'error',
        title: 'Usuário sem permissão para acessar essa página.',
      });
      history.push('/dashboard');
    }
  }, [user, addToast]);

  useEffect(() => {
    if (simulates && simulates.length) {
      // eslint-disable-next-line prefer-const
      let newCategories = [];
      simulates.forEach(category => {
        if (
          !newCategories.find(ctgry => ctgry.key === category.key) &&
          category.category.value === id
        ) {
          newCategories.push(category);
        }
      });
      setSimulateData(newCategories);
    } else {
      setSimulateData([]);
    }
  }, [simulates, id]);

  return (
    <Container>
      <PaperStyled>
        <ButtonStyled
          variant="contained"
          color="primary"
          style={{
            background: colors.orange,
          }}
          onClick={() => setOpen(!open)}
        >
          <IconStyled>add</IconStyled>

          {language ? 'Add sub category' : 'Novo simulado'}
        </ButtonStyled>

        <Table
          data={simulateData}
          onChangeSearch={handleChangeSearch}
          search={handleSearch}
          alteredStatus={handleStatus}
          handleGetItem={handleGetItem}
        />
      </PaperStyled>

      <ModalItem
        handleCloseModal={handleCloseModal}
        open={open}
        categoryId={id}
        getItens={getSimulate}
        item={item}
      />
    </Container>
  );
};

Item.propTypes = {
  match: PropTypes.node.isRequired,
};

export default Item;
