/* eslint-disable array-callback-return */
/* eslint-disable func-names */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-boolean-value */
import React, { useCallback, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import Select from 'react-select';
import { format } from 'date-fns';
import {
  Typography,
  Grid,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Checkbox,
} from '@material-ui/core';

import firebase from '~/services/firebase';
import { useAuth } from '~/hooks/Auth';
import { useToast } from '~/hooks/Toast';
import LoadingPage from '~/components/Loader';
import { useSimulateMain } from '~/hooks/SimulateMain';

import { TextFieldStyled, ErrorMessageText } from './styles';

const ModalItem = ({
  handleCloseModal,
  open,
  item = null,
  getItens,
  categoryId,
}) => {
  const { simulateMain, getSimulateMain } = useSimulateMain();
  const { addToast } = useToast();
  const [loading, setLoading] = useState(false);
  const { language } = useAuth();
  const [categoryData, setCategoryData] = useState([]);

  const schema = Yup.object().shape({
    name: Yup.string().required(
      language
        ? 'Enter the name of the sub group.'
        : 'Informe o título do sub grupo'
    ),
  });

  const handleSaveClasse = useCallback(
    async data => {
      setLoading(true);
      if (item && item.key) {
        firebase
          .firestore()
          .collection('questions_simulate')
          .doc(item.key)
          .update({
            ...data,
            simulate: categoryData[0],
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('questions_simulate')
          .add({
            ...data,
            simulate: categoryData[0],
            date: format(new Date(), 'yyyy-MM-dd')
              .concat(' ')
              .concat(format(new Date(), 'HH:mm:ss')),
            active: true,
          })
          .then(() => {
            setLoading(false);
            addToast({
              type: 'success',
              title: language ? 'Registered successfully' : 'Salvo com sucesso',
            });
            handleCloseModal();
            getItens();
            setLoading(false);
          })
          .catch(function () {
            addToast({
              type: 'error',
              title: 'Tente novamente',
            });
            setLoading(false);
          });
      }
    },
    [addToast, getItens, handleCloseModal, item, language, categoryData]
  );

  useEffect(() => {
    getSimulateMain();
  }, [getSimulateMain]);

  useEffect(() => {
    async function getCategoriesData() {
      try {
        setLoading(true);
        const arr = [];
        simulateMain.map(category => {
          if (category.active && category.key === categoryId) {
            const objData = {
              value: category.key,
              label: category.name,
            };
            arr.push(objData);
          }
        });
        setCategoryData(arr);
        setLoading(false);
      } catch (err) {
        setLoading(false);
        addToast({
          type: 'error',
          title: language ? 'An error has occurred.' : 'Ocorreu um erro.',
        });
      }
    }
    getCategoriesData();
  }, [addToast, language, simulateMain, categoryId]);

  return (
    <Dialog aria-labelledby="customized-dialog-title" open={open}>
      <DialogTitle onClose={() => handleCloseModal()}>
        <div>
          <Typography variant="h6">
            {language ? 'Sub Group' : 'Sub Grupo'}{' '}
          </Typography>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflowX: 'hidden' }}>
        <Formik
          enableReinitialize
          validationSchema={schema}
          initialValues={{
            name: item && item.name ? item.name : '',
            simulate: item && item.simulate ? item.simulate : '',
            alternative_a: item && item.alternative_a ? item.alternative_a : '',
            is_correct_a: item && item.is_correct_a ? item.is_correct_a : false,
            alternative_b: item && item.alternative_b ? item.alternative_b : '',
            is_correct_b: item && item.is_correct_b ? item.is_correct_b : false,
            alternative_c: item && item.alternative_c ? item.alternative_c : '',
            is_correct_c: item && item.is_correct_c ? item.is_correct_c : false,
            alternative_d: item && item.alternative_d ? item.alternative_d : '',
            is_correct_d: item && item.is_correct_d ? item.is_correct_d : false,
            description_answer:
              item && item.description_answer ? item.description_answer : '',
            description: item && item.description ? item.description : '',
          }}
          onSubmit={handleSaveClasse}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            errors,
            touched,
            setFieldValue,
          }) => (
            <Form>
              <Grid container>
                <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                  <TextFieldStyled
                    value={values.name}
                    fullWidth
                    id="name"
                    name="name"
                    required
                    onChange={handleChange}
                    label={language ? 'Name' : 'Título da questão'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.name && touched.name ? (
                    <ErrorMessageText>{errors.name}</ErrorMessageText>
                  ) : null}
                </Grid>
                <Grid
                  item
                  xs={12}
                  sm={12}
                  md={12}
                  style={{ padding: 8, zIndex: 1000 }}
                >
                  <span>Simulado</span>
                  <Select
                    options={categoryData}
                    value={values.simulate || categoryData[0]}
                    isDisabled
                    placeholder="Simulado"
                    style={{ background: 'white', zIndex: 999 }}
                    label="Simulado"
                    onChange={e => {
                      setFieldValue('simulate', e);
                    }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                  <TextFieldStyled
                    value={values.description}
                    fullWidth
                    id="description"
                    name="description"
                    required
                    rows={4}
                    multiline
                    onChange={handleChange}
                    label={language ? 'Name' : 'Questão'}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  {errors.description && touched.description ? (
                    <ErrorMessageText>{errors.description}</ErrorMessageText>
                  ) : null}
                </Grid>
                <h4 style={{ paddingLeft: 8, paddingTop: 16 }}>Respostas</h4>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_a}
                      fullWidth
                      id="alternative_a"
                      name="alternative_a"
                      required
                      onChange={handleChange}
                      label="Alternativa A"
                      style={{ marginBottom: 0 }}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_a && touched.alternative_a ? (
                      <ErrorMessageText>
                        {errors.alternative_a}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_b}
                      fullWidth
                      id="alternative_b"
                      name="alternative_b"
                      required
                      onChange={handleChange}
                      label="Alternativa B"
                      variant="outlined"
                      style={{ marginBottom: 0 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_b && touched.alternative_b ? (
                      <ErrorMessageText>
                        {errors.alternative_b}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_c}
                      fullWidth
                      id="alternative_c"
                      name="alternative_c"
                      required
                      onChange={handleChange}
                      label="Alternativa C"
                      variant="outlined"
                      style={{ marginBottom: 0 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_c && touched.alternative_c ? (
                      <ErrorMessageText>
                        {errors.alternative_c}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
                <Grid container style={{ marginBottom: 24 }}>
                  <Grid item xs={12} sm={12} md={12} style={{ minWidth: 500 }}>
                    <TextFieldStyled
                      value={values.alternative_d}
                      fullWidth
                      id="alternative_d"
                      name="alternative_d"
                      required
                      onChange={handleChange}
                      label="Alternativa D"
                      variant="outlined"
                      style={{ marginBottom: 0 }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.alternative_d && touched.alternative_d ? (
                      <ErrorMessageText>
                        {errors.alternative_d}
                      </ErrorMessageText>
                    ) : null}
                  </Grid>
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={12} sm={12} md={12} style={{ paddingLeft: 8 }}>
                  <span>Resposta Correta</span>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_a}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_a', !values.is_correct_a);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>A</Typography>
                  </div>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_b}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_b', !values.is_correct_b);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>B</Typography>
                  </div>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_c}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_c', !values.is_correct_c);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>C</Typography>
                  </div>
                </Grid>
                <Grid item xs={3} sm={3} md={3}>
                  <div style={{ display: 'flex', alignItems: 'center' }}>
                    <Checkbox
                      checked={values.is_correct_d}
                      color="primary"
                      onChange={() => {
                        setFieldValue('is_correct_d', !values.is_correct_d);
                      }}
                      inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                    <Typography>D</Typography>
                  </div>
                </Grid>
              </Grid>
              {item && (
                <Grid item xs={12}>
                  <TextFieldStyled
                    value={values.active}
                    fullWidth
                    id="active"
                    name="active"
                    required
                    onChange={handleChange}
                    label="Status"
                    select
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    <option key={true} value={true}>
                      {language ? 'Active' : 'Ativo'}
                    </option>
                    <option key={false} value={false}>
                      {language ? 'Inactive' : 'Inativo'}
                    </option>
                  </TextFieldStyled>
                </Grid>
              )}
              <Grid container>
                <Grid item xs={12} sm={12} md={12}>
                  <Grid container justify="flex-end">
                    <DialogActions>
                      <Button
                        onClick={() => handleSubmit()}
                        color="primary"
                        variant="contained"
                        disabled={loading}
                      >
                        {language ? 'Save' : 'Salvar'}
                      </Button>
                    </DialogActions>
                    <DialogActions>
                      <Button
                        variant="outlined"
                        onClick={() => handleCloseModal()}
                        color="primary"
                      >
                        {language ? 'Cancel' : 'Cancelar'}
                      </Button>
                    </DialogActions>
                  </Grid>
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
        {loading && (
          <div
            style={{
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <LoadingPage size={30} loading={loading} type="ThreeDots" />
          </div>
        )}
      </DialogContent>
    </Dialog>
  );
};

ModalItem.propTypes = {
  handleCloseModal: PropTypes.func.isRequired,
  getItens: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  item: PropTypes.node.isRequired,
  categoryId: PropTypes.string.isRequired,
};

export default ModalItem;
