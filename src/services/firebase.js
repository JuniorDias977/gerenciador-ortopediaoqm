import firebase from 'firebase';
import 'firebase/storage';

const config = {
  // apiKey: 'AIzaSyB1cwQj4OiQhZ36GmafStB6OZKHBtDbhy4',
  // authDomain: 'ortopedia-10e76.firebaseapp.com',
  // projectId: 'ortopedia-10e76',
  // storageBucket: 'ortopedia-10e76.appspot.com',
  // messagingSenderId: '655428816414',
  // appId: '1:655428816414:web:6c75a03372ede71160680f',
  // measurementId: 'G-SQ14TNQCYC',
  apiKey: 'AIzaSyDfeae7wtvHcHeAk0StPCD87KS1Yfsx8_g',
  authDomain: 'ortopedia-b6793.firebaseapp.com',
  projectId: 'ortopedia-b6793',
  storageBucket: 'ortopedia-b6793.appspot.com',
  messagingSenderId: '657839954573',
  appId: '1:657839954573:web:48d0624734fba2f52f20c4',
  measurementId: 'G-YMB0GV2QKE',
};
firebase.initializeApp(config);
firebase.analytics();
const storage = firebase.storage();
export { storage, firebase as default };
