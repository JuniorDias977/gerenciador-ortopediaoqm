import React, { createContext, useCallback, useState, useContext } from 'react';
import PropTypes from 'prop-types';

import firebase from '~/services/firebase';

require('dotenv/config');

const AuthContext = createContext({});

export function useAuth() {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used whitin an AuthProvider');
  }

  return context;
}

export const AuthProvider = ({ children }) => {
  const [loading, setLoading] = useState(false);

  const [data, setData] = useState(() => {
    const user = localStorage.getItem('@ortopediaWeb:user');
    const language = localStorage.getItem('@ortopediaWeb:language');
    if (user && language) {
      return { user: JSON.parse(user), language: JSON.parse(language) };
    }
    if (user) {
      return { user: JSON.parse(user) };
    }
    if (language) {
      return { language: JSON.parse(language) };
    }
    return {};
  });

  const signOut = useCallback(() => {
    localStorage.removeItem('@ortopediaWeb:user');
    localStorage.removeItem('@ortopediaWeb:language');
    setData({});
  }, []);

  const updateLanguage = useCallback(() => {
    setData(() => {
      const user = localStorage.getItem('@ortopediaWeb:user');
      const language = localStorage.getItem('@ortopediaWeb:language');
      if (user && language) {
        return {
          user: JSON.parse(user),
          language: JSON.parse(language),
        };
      }
      if (user) {
        return { user: JSON.parse(user) };
      }
      if (language) {
        return { language: JSON.parse(language) };
      }
      return {};
    });
  }, []);

  const signIn = useCallback(async ({ email, password }, setError) => {
    setLoading(true);
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      // eslint-disable-next-line func-names
      .then(async function (user) {
        firebase
          .firestore()
          .collection('users')
          // Filter results
          .where('email', '==', user.user.email)
          .get()
          .then(async querySnapshot => {
            // eslint-disable-next-line prefer-const
            let usersArr = [];
            querySnapshot.forEach(documentSnapshot => {
              usersArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setLoading(false);
            if (usersArr && usersArr.length) {
              if (usersArr[0].level === 'admin') {
                localStorage.setItem(
                  '@ortopediaWeb:user',
                  JSON.stringify(usersArr[0])
                );
                setData({ user });
                setLoading(false);

                setData({ user: usersArr[0] });
              } else {
                setError('Email e senha inválidos');
              }
            }
          });
      })
      .catch(() => {
        setLoading(false);
        setError('Email e senha inválidos');
      });
  }, []);
  return (
    <AuthContext.Provider
      value={{
        user: data.user,
        language: data.language,
        signIn,
        signOut,
        loading,
        setLoading,
        updateLanguage,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.shape().isRequired,
};
