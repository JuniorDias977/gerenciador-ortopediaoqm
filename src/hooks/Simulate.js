/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const SimulateContext = createContext({});

export function useSimulate() {
  const context = useContext(SimulateContext);

  if (!context) {
    throw new Error('useSimulate must be used whitin an SimulateProvider');
  }

  return context;
}

export const SimulateProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [simulates, setSimulate] = useState([]);

  const getSimulate = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('simulates')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSimulate(categoriesArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('simulates')
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSimulate(categoriesArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getSimulate();
  }, [getSimulate]);

  return (
    <SimulateContext.Provider
      value={{
        simulates,
        loadingSimulate: loading,
        getSimulate,
      }}
    >
      {children}
    </SimulateContext.Provider>
  );
};

SimulateProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
