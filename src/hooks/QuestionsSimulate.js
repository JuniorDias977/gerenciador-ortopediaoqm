/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const QuestionsSimulateContext = createContext({});

export function useQuestionsSimulate() {
  const context = useContext(QuestionsSimulateContext);

  if (!context) {
    throw new Error(
      'useQuestionsSimulate must be used whitin an QuestionsSimulateProvider'
    );
  }

  return context;
}

export const QuestionsSimulateProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [questionsSimulate, setQuestionsSimulate] = useState([]);

  const getQuestionsSimulate = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('questions_simulate')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let questionsSimulateArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsSimulateArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestionsSimulate(questionsSimulateArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('questions_simulate')
          .onSnapshot(querySnapshot => {
            let questionsSimulateArr = [];
            querySnapshot.forEach(documentSnapshot => {
              questionsSimulateArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestionsSimulate(questionsSimulateArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getQuestionsSimulate();
  }, [getQuestionsSimulate]);

  return (
    <QuestionsSimulateContext.Provider
      value={{
        questionsSimulate,
        loadingQuestionsSimulate: loading,
        getQuestionsSimulate,
      }}
    >
      {children}
    </QuestionsSimulateContext.Provider>
  );
};

QuestionsSimulateProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
