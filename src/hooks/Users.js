/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const UsersContext = createContext({});

export function useUsers() {
  const context = useContext(UsersContext);

  if (!context) {
    throw new Error('useUsers must be used whitin an UsersProvider');
  }

  return context;
}

export const UsersProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [users, setUsers] = useState([]);

  const getUsers = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('users')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let usersArr = [];
            querySnapshot.forEach(documentSnapshot => {
              usersArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setUsers(usersArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('users')
          .onSnapshot(querySnapshot => {
            let usersArr = [];
            querySnapshot.forEach(documentSnapshot => {
              usersArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setUsers(usersArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getUsers();
  }, [getUsers]);

  return (
    <UsersContext.Provider
      value={{
        users,
        loadingUsers: loading,
        getUsers,
      }}
    >
      {children}
    </UsersContext.Provider>
  );
};

UsersProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
