/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const SimulateMainContext = createContext({});

export function useSimulateMain() {
  const context = useContext(SimulateMainContext);

  if (!context) {
    throw new Error(
      'useSimulateMain must be used whitin an SimulateMainProvider'
    );
  }

  return context;
}

export const SimulateMainProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [simulateMain, setSimulateMain] = useState([]);

  const getSimulateMain = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('simulate_main')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let simulateMainArr = [];
            querySnapshot.forEach(documentSnapshot => {
              simulateMainArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSimulateMain(simulateMainArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('simulate_main')
          .onSnapshot(querySnapshot => {
            let simulateMainArr = [];
            querySnapshot.forEach(documentSnapshot => {
              simulateMainArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSimulateMain(simulateMainArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getSimulateMain();
  }, [getSimulateMain]);

  return (
    <SimulateMainContext.Provider
      value={{
        simulateMain,
        loadingSimulateMain: loading,
        getSimulateMain,
      }}
    >
      {children}
    </SimulateMainContext.Provider>
  );
};

SimulateMainProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
