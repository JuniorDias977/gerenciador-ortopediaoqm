/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const TagsContext = createContext({});

export function useTags() {
  const context = useContext(TagsContext);

  if (!context) {
    throw new Error('useTags must be used whitin an TagsProvider');
  }

  return context;
}

export const TagsProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [tags, setTags] = useState([]);

  const getTags = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('tags')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let tagsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              tagsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setTags(tagsArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('tags')
          .onSnapshot(querySnapshot => {
            let tagsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              tagsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setTags(tagsArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getTags();
  }, [getTags]);

  return (
    <TagsContext.Provider
      value={{
        tags,
        loadingTags: loading,
        getTags,
      }}
    >
      {children}
    </TagsContext.Provider>
  );
};

TagsProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
