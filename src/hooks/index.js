import React from 'react';
import PropTypes from 'prop-types';

import { AuthProvider } from './Auth';
import { ToastProvider } from './Toast';
import { UsersProvider } from './Users';
import { CategoriesProvider } from './Categories';
import { SubCategoriesProvider } from './SubCategories';
import { SubGroupsProvider } from './SubGroups';
import { TagsProvider } from './Tags';
import { QuestionsProvider } from './Questions';
import { CategoriesSimulateProvider } from './CategoriesSimulate';
import { SimulateProvider } from './Simulate';
import { QuestionsSimulateProvider } from './QuestionsSimulate';
import { SimulateMainProvider } from './SimulateMain';

const AppProvider = ({ children }) => {
  return (
    <AuthProvider>
      <ToastProvider>
        <UsersProvider>
          <SimulateMainProvider>
            <CategoriesSimulateProvider>
              <QuestionsSimulateProvider>
                <SimulateProvider>
                  <CategoriesProvider>
                    <SubCategoriesProvider>
                      <SubGroupsProvider>
                        <TagsProvider>
                          <QuestionsProvider>{children}</QuestionsProvider>
                        </TagsProvider>
                      </SubGroupsProvider>
                    </SubCategoriesProvider>
                  </CategoriesProvider>
                </SimulateProvider>
              </QuestionsSimulateProvider>
            </CategoriesSimulateProvider>
          </SimulateMainProvider>
        </UsersProvider>
      </ToastProvider>
    </AuthProvider>
  );
};

AppProvider.propTypes = {
  children: PropTypes.shape().isRequired,
};

export default AppProvider;
