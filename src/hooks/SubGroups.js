/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const SubGroupsContext = createContext({});

export function useSubGroups() {
  const context = useContext(SubGroupsContext);

  if (!context) {
    throw new Error('useSubGroups must be used whitin an SubGroupsProvider');
  }

  return context;
}

export const SubGroupsProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [subGroups, setSubGroups] = useState([]);

  const getSubGroups = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('sub_groups')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let subGroupsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              subGroupsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSubGroups(subGroupsArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('sub_groups')
          .onSnapshot(querySnapshot => {
            let subGroupsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              subGroupsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSubGroups(subGroupsArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getSubGroups();
  }, [getSubGroups]);

  return (
    <SubGroupsContext.Provider
      value={{
        subGroups,
        loadingsubGroups: loading,
        getSubGroups,
      }}
    >
      {children}
    </SubGroupsContext.Provider>
  );
};

SubGroupsProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
