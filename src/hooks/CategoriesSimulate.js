/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const CategoriesSimulateContext = createContext({});

export function useCategoriesSimulate() {
  const context = useContext(CategoriesSimulateContext);

  if (!context) {
    throw new Error(
      'useCategoriesSimulate must be used whitin an CategoriesSimulateProvider'
    );
  }

  return context;
}

export const CategoriesSimulateProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [categoriesSimulate, setCategoriesSimulate] = useState([]);

  const getCategoriesSimulate = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('categories_simulate')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setCategoriesSimulate(categoriesArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('categories_simulate')
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setCategoriesSimulate(categoriesArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getCategoriesSimulate();
  }, [getCategoriesSimulate]);

  return (
    <CategoriesSimulateContext.Provider
      value={{
        categoriesSimulate,
        loadingCategories: loading,
        getCategoriesSimulate,
      }}
    >
      {children}
    </CategoriesSimulateContext.Provider>
  );
};

CategoriesSimulateProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
