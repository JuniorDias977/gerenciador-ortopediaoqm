/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const QuestionsContext = createContext({});

export function useQuestions() {
  const context = useContext(QuestionsContext);

  if (!context) {
    throw new Error('useQuestions must be used whitin an QuestionsProvider');
  }

  return context;
}

export const QuestionsProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [questions, setQuestions] = useState([]);

  const getQuestions = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('questions')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let QuestionsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              QuestionsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestions(QuestionsArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('questions')
          .onSnapshot(querySnapshot => {
            let QuestionsArr = [];
            querySnapshot.forEach(documentSnapshot => {
              QuestionsArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setQuestions(QuestionsArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getQuestions();
  }, [getQuestions]);

  return (
    <QuestionsContext.Provider
      value={{
        questions,
        loadingQuestions: loading,
        getQuestions,
      }}
    >
      {children}
    </QuestionsContext.Provider>
  );
};

QuestionsProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
