/* eslint-disable prefer-const */
import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useEffect,
} from 'react';
import PropTypes from 'prop-types';
import firebase from '~/services/firebase';

const SubCategoriesContext = createContext({});

export function useSubCategories() {
  const context = useContext(SubCategoriesContext);

  if (!context) {
    throw new Error(
      'useSubCategories must be used whitin an SubCategoriesProvider'
    );
  }

  return context;
}

export const SubCategoriesProvider = ({ children }) => {
  const [loading, setLoading] = useState(true);
  const [subCategories, setSubCategories] = useState([]);

  const getSubCategories = useCallback(search => {
    setLoading(true);
    try {
      if (search) {
        firebase
          .firestore()
          .collection('sub_categories')
          .where('name', '==', search)
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSubCategories(categoriesArr);
            setLoading(false);
          });
      } else {
        firebase
          .firestore()
          .collection('sub_categories')
          .onSnapshot(querySnapshot => {
            let categoriesArr = [];
            querySnapshot.forEach(documentSnapshot => {
              categoriesArr.push({
                ...documentSnapshot.data(),
                key: documentSnapshot.id,
              });
            });
            setSubCategories(categoriesArr);
            setLoading(false);
          });
      }

      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    getSubCategories();
  }, [getSubCategories]);

  return (
    <SubCategoriesContext.Provider
      value={{
        subCategories,
        loadingSubCategories: loading,
        getSubCategories,
      }}
    >
      {children}
    </SubCategoriesContext.Provider>
  );
};

SubCategoriesProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
