export const toBoolean = stringValue => {
  return stringValue === 'true' || stringValue === true;
};
