import { toBoolean } from '../values';

describe('Values', function () {
  it('should string value converts to boolean', () => {
    const trueValue = toBoolean('true');
    const falseValue = toBoolean('false');

    expect(trueValue).toEqual(true);
    expect(falseValue).toEqual(false);
  });

  it('should return boolean is value is boolean', () => {
    const trueValue = toBoolean(true);
    const falseValue = toBoolean(false);

    expect(trueValue).toEqual(true);
    expect(falseValue).toEqual(false);
  });
});
